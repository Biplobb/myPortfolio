-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2017 at 06:27 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `portfolio`
--

-- --------------------------------------------------------

--
-- Table structure for table `basic_info`
--

CREATE TABLE `basic_info` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_slug` varchar(255) NOT NULL,
  `age` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `freelance` varchar(255) NOT NULL,
  `vacation` varchar(255) NOT NULL,
  `facebook` varchar(1555) NOT NULL,
  `twitter` varchar(1555) NOT NULL,
  `linkedin` varchar(1555) NOT NULL,
  `instragram` varchar(1555) NOT NULL,
  `cart` varchar(1555) NOT NULL,
  `download` varchar(1555) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` datetime NOT NULL,
  `unique_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `basic_info`
--

INSERT INTO `basic_info` (`id`, `name`, `name_slug`, `age`, `address`, `email`, `password`, `phone`, `img`, `freelance`, `vacation`, `facebook`, `twitter`, `linkedin`, `instragram`, `cart`, `download`, `status`, `created_at`, `modified_at`, `deleted_at`, `unique_id`) VALUES
(43, 'MD NUR-UN NABI BIPLOB', 'I AM PHP DEVELOPER AND LARAVEL DEVELOPER', '22', 'West rajarBazar,Dhaka', 'nbiplob15@gmail.com', '59841433', '01631514586', '40ae9290.jpg', '11 march', '12 april', 'https://www.facebook.com/profile.php?id=100007787220986&ref=br_rs', 'https://www.twitter.com/', 'https://www.linkedin.com/in/nur-un-nabi-biplob-381566152/', 'https://www.instagram.com/nbiplob_15/', 'https://gitlab.com/Biplobb/project', 'This is awesome templete', 0, '2017-11-07 06:12:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 717);

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `viber` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `unique_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `email`, `phone`, `viber`, `facebook`, `address`, `status`, `unique_id`) VALUES
(2, 'nbiplob15@gmail.com', '01631514586', '01631514586', '01714740719', 'West rajarBazar,Dhaka', 0, 7518);

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE `education` (
  `id` int(11) NOT NULL,
  `school` varchar(255) NOT NULL,
  `college` varchar(255) NOT NULL,
  `university` varchar(255) NOT NULL,
  `other` varchar(255) NOT NULL,
  `unique_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `education`
--

INSERT INTO `education` (`id`, `school`, `college`, `university`, `other`, `unique_id`) VALUES
(4, 'Khordo M.L high school ', 'Comilla cantonment  college', 'Daffodil International University', 'web development-PHP 3 month course', 'c9b7e006e053c5600afe9d20a26dd83a');

-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE `experience` (
  `id` int(11) NOT NULL,
  `title1` varchar(255) NOT NULL,
  `session1` varchar(255) NOT NULL,
  `details1` varchar(1555) NOT NULL,
  `title2` varchar(255) NOT NULL,
  `session2` varchar(255) NOT NULL,
  `details2` varchar(1555) NOT NULL,
  `title3` varchar(255) NOT NULL,
  `session3` varchar(255) NOT NULL,
  `details3` varchar(1555) NOT NULL,
  `title4` varchar(255) NOT NULL,
  `session4` varchar(255) NOT NULL,
  `details4` varchar(1555) NOT NULL,
  `unique_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `experience`
--

INSERT INTO `experience` (`id`, `title1`, `session1`, `details1`, `title2`, `session2`, `details2`, `title3`, `session3`, `details3`, `title4`, `session4`, `details4`, `unique_id`) VALUES
(2, 'web design', '2002-2004', 'i am web designer', 'java', '2004-2006', 'i am java developer', 'laravel', '2006-2008', 'i am laravel framework developer', 'Php', '2008-2017', 'i am php developer', 'fef37eee50452d2fbcb8594fd6f9236f');

-- --------------------------------------------------------

--
-- Table structure for table `interests`
--

CREATE TABLE `interests` (
  `id` int(11) NOT NULL,
  `interest1` varchar(255) NOT NULL,
  `interest1_img` varchar(1555) NOT NULL,
  `interest2` varchar(255) NOT NULL,
  `interest2_img` varchar(1555) NOT NULL,
  `interest3` varchar(1555) NOT NULL,
  `interest3_img` varchar(1555) NOT NULL,
  `interest4` varchar(255) NOT NULL,
  `interest4_img` varchar(1555) NOT NULL,
  `interest5` varchar(255) NOT NULL,
  `interest5_img` varchar(1555) NOT NULL,
  `unique_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `interests`
--

INSERT INTO `interests` (`id`, `interest1`, `interest1_img`, `interest2`, `interest2_img`, `interest3`, `interest3_img`, `interest4`, `interest4_img`, `interest5`, `interest5_img`, `unique_id`) VALUES
(7, 'Bicycling', '132c2370.jpg', 'Swimming', '132c2370.jpg', 'Gossiping', '132c2370.jpg', 'shopping', '132c2370.jpg', 'watch movies', '132c2370.jpg', '132c237076452ea865c7a91d63f3fd6c');

-- --------------------------------------------------------

--
-- Table structure for table `professional_skill`
--

CREATE TABLE `professional_skill` (
  `id` int(11) NOT NULL,
  `skill1` varchar(255) NOT NULL,
  `percentage1` varchar(255) NOT NULL,
  `skill2` varchar(255) NOT NULL,
  `percentage2` varchar(255) NOT NULL,
  `skill3` varchar(255) NOT NULL,
  `percentage3` varchar(255) NOT NULL,
  `skill4` varchar(255) NOT NULL,
  `percentage4` varchar(255) NOT NULL,
  `skill5` varchar(255) NOT NULL,
  `percentage5` varchar(255) NOT NULL,
  `skill6` varchar(255) NOT NULL,
  `percentage6` varchar(255) NOT NULL,
  `unique_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `professional_skill`
--

INSERT INTO `professional_skill` (`id`, `skill1`, `percentage1`, `skill2`, `percentage2`, `skill3`, `percentage3`, `skill4`, `percentage4`, `skill5`, `percentage5`, `skill6`, `percentage6`, `unique_id`) VALUES
(2, 'php', '80', 'java', '50', 'web dewsign', '60', 'web development', '90', 'laravel development', '30', 'c programming', '70', 'fb06edf19fc65f0f1c081d64be217a44');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `title1` varchar(255) NOT NULL,
  `fulltitle1` varchar(1555) NOT NULL,
  `details1` varchar(1555) NOT NULL,
  `title2` varchar(255) NOT NULL,
  `fulltitle2` varchar(1555) NOT NULL,
  `details2` varchar(1555) NOT NULL,
  `title3` varchar(255) NOT NULL,
  `fulltitle3` varchar(1555) NOT NULL,
  `details3` varchar(1555) NOT NULL,
  `title4` varchar(255) NOT NULL,
  `fulltitle4` varchar(1555) NOT NULL,
  `details4` varchar(1555) NOT NULL,
  `unique_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `title1`, `fulltitle1`, `details1`, `title2`, `fulltitle2`, `details2`, `title3`, `fulltitle3`, `details3`, `title4`, `fulltitle4`, `details4`, `unique_id`) VALUES
(4, ' design', 'web design', 'Herbs are fun and easy to grow. When harvested they make even the simplest meal seem like a gourmet delight. By using herbs in your cooking you can', 'java', 'object oriented programming', 'i am java developer', 'laravel', ' laravel framework', 'i am laravel framework developer', 'Php', 'web developer', 'i am php developer', '60238b3a38b226e402765eb11f9afb20'),
(5, ' design', 'web design', 'Herbs are fun and easy to grow. When harvested they make even the simplest meal seem like a gourmet delight. By using herbs in your cooking you can', 'java', 'object oriented programming', 'i am java developer', 'laravel', ' laravel framework', 'i am laravel framework developer', 'Php', 'web developer', 'i am php developer', '35ce62843db7ecc902978be7c2881d86'),
(6, 'design', 'web design', 'i am web designer', 'java', 'object oriented programming', 'i am java developer', 'laravel', 'laravel framework', 'i am laravel framework developer', 'Php', 'web developer', 'i am php developer', '00aa8dec19a1374ed58189f07c7f0972');

-- --------------------------------------------------------

--
-- Table structure for table `toogle_section`
--

CREATE TABLE `toogle_section` (
  `id` int(11) NOT NULL,
  `question` varchar(1055) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toogle_section`
--

INSERT INTO `toogle_section` (`id`, `question`) VALUES
(1, 'Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `basic_info`
--
ALTER TABLE `basic_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `education`
--
ALTER TABLE `education`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `experience`
--
ALTER TABLE `experience`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interests`
--
ALTER TABLE `interests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `professional_skill`
--
ALTER TABLE `professional_skill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toogle_section`
--
ALTER TABLE `toogle_section`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `basic_info`
--
ALTER TABLE `basic_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `education`
--
ALTER TABLE `education`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `experience`
--
ALTER TABLE `experience`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `interests`
--
ALTER TABLE `interests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `professional_skill`
--
ALTER TABLE `professional_skill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `toogle_section`
--
ALTER TABLE `toogle_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
