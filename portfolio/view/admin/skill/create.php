<?php
include_once '../includes/header.php';
?>



    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Blank page
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Blank page</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Professional skill Form</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="view/admin/skill/store.php" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">skill-1</label>

                                <div class="col-sm-10">
                                    <input name="skill1" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Percentage-1</label>

                                <div class="col-sm-10">
                                    <input name="percentage1" type="text" class="form-control" >
                                </div>
                            </div>


                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">skill-2</label>

                                <div class="col-sm-10">
                                    <input name="skill2" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Percentage-2</label>

                                <div class="col-sm-10">
                                    <input name="percentage2" type="text" class="form-control" >
                                </div>
                            </div>


                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">skill-3</label>

                                <div class="col-sm-10">
                                    <input name="skill3" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Percentage-3</label>

                                <div class="col-sm-10">
                                    <input name="percentage3" type="text" class="form-control" >
                                </div>
                            </div>


                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">skill-4</label>

                                <div class="col-sm-10">
                                    <input name="skill4" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Percentage-4</label>

                                <div class="col-sm-10">
                                    <input name="percentage4" type="text" class="form-control" >
                                </div>
                            </div>


                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">skill-5</label>

                                <div class="col-sm-10">
                                    <input name="skill5" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Percentage-5</label>

                                <div class="col-sm-10">
                                    <input name="percentage5" type="text" class="form-control" >
                                </div>
                            </div>


                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">skill-6</label>

                                <div class="col-sm-10">
                                    <input name="skill6" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Percentage-6</label>

                                <div class="col-sm-10">
                                    <input name="percentage6" type="text" class="form-control" >
                                </div>
                            </div>


                        </div>



                        <!-- /.box-body -->
                        <div class="box-footer">

                            <button type="submit" class="btn btn-success">submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>

                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->


    </div>





<?php
include_once '../includes/footer.php';
?>