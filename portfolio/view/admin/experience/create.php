<?php
include_once '../includes/header.php';
?>



    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Blank page
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Blank page</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Basic Info From</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="view/admin/experience/store.php" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Title-1</label>

                                <div class="col-sm-10">
                                    <input name="title1" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Session-1</label>

                                <div class="col-sm-10">
                                    <input name="session1" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Details-1</label>

                                <div class="col-sm-10">
                                    <input name="details1" type="text" class="form-control" >
                                </div>
                            </div>


                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Title-2</label>

                                <div class="col-sm-10">
                                    <input name="title2" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Session-2</label>

                                <div class="col-sm-10">
                                    <input name="session2" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Details-2</label>

                                <div class="col-sm-10">
                                    <input name="details2" type="text" class="form-control" >
                                </div>
                            </div>


                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Title-3</label>

                                <div class="col-sm-10">
                                    <input name="title3" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Session-3</label>

                                <div class="col-sm-10">
                                    <input name="session3" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Details-3</label>

                                <div class="col-sm-10">
                                    <input name="details3" type="text" class="form-control" >
                                </div>
                            </div>


                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Title-4</label>

                                <div class="col-sm-10">
                                    <input name="title4" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Session-4</label>

                                <div class="col-sm-10">
                                    <input name="session4" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Details-4</label>

                                <div class="col-sm-10">
                                    <input name="details4" type="text" class="form-control" >
                                </div>
                            </div>


                        </div>

                        <!-- /.box-body -->
                        <div class="box-footer">

                            <button type="submit" class="btn btn-success">submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>

                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->


    </div>





<?php
include_once '../includes/footer.php';
?>