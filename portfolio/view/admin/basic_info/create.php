<?php
include_once '../includes/header.php';
?>



    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Blank page
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Blank page</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Basic Info From</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="view/admin/basic_info/store.php" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Name</label>

                                <div class="col-sm-10">
                                    <input name="name" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Name slug</label>

                                <div class="col-sm-10">
                                    <input name="name_slug" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Age</label>

                                <div class="col-sm-10">
                                    <input name="age" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Address</label>

                                <div class="col-sm-10">
                                    <input name="address" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Email</label>

                                <div class="col-sm-10">
                                    <input name="email" type="email" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Phone</label>

                                <div class="col-sm-10">
                                    <input name="phone" type="text" class="form-control" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Freelance</label>

                                <div class="col-sm-10">
                                    <input name="freelance" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Vacation</label>

                                <div class="col-sm-10">
                                    <input name="vacation" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Facebook Link</label>

                                <div class="col-sm-10">
                                    <input name="facebook" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Twitter Link</label>

                                <div class="col-sm-10">
                                    <input name="twitter" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Linkedin link</label>

                                <div class="col-sm-10">
                                    <input name="linkedin" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Instragram Link</label>

                                <div class="col-sm-10">
                                    <input name="instragram" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">cart</label>

                                <div class="col-sm-10">
                                    <input name="cart" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Download Resume </label>

                                <div class="col-sm-10">
                                    <input name="download" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Image</label>

                                <div class="col-sm-10">
                                    <input name="img" type="file" class="form-control" >
                                </div>
                            </div>


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">

                            <button type="submit" class="btn btn-success">submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>

                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->


    </div>





<?php
include_once '../includes/footer.php';
?>