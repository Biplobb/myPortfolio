<?php
include_once '../includes/header.php';
include_once '../../../vendor/autoload.php';
$basic=new \App\admin\basicInfo();
$data=$basic->show($_GET['id']);

?>



    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Blank page
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Blank page</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Basic Info From</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="view/admin/basic_info/update.php" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Name</label>

                                <div class="col-sm-10">
                                    <input name="name" value="<?php echo $data['name']?>" type="text" class="form-control" >
                                    <input name="id" type="hidden" value="<?php echo $data['id']?>" type="text" class="form-control" >
                                    <input name="img" type="hidden" value="<?php echo $data['img']?>" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Name slug</label>

                                <div class="col-sm-10">
                                    <input name="name_slug" value="<?php echo $data['name_slug']?>" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Age</label>

                                <div class="col-sm-10">
                                    <input name="age" value="<?php echo $data['age']?>" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Address</label>

                                <div class="col-sm-10">
                                    <input name="address" value="<?php echo $data['address']?>" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Email</label>

                                <div class="col-sm-10">
                                    <input name="email" value="<?php echo $data['email']?>" type="email" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Phone</label>

                                <div class="col-sm-10">
                                    <input name="phone" value="<?php echo $data['phone']?>" type="text" class="form-control" >
                                </div>

                            </div>

                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Freelance</label>

                                <div class="col-sm-10">
                                    <input name="freelance" value="<?php echo $data['freelance']?>" type="text" class="form-control" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Vacation</label>

                                <div class="col-sm-10">
                                    <input name="vacation" value="<?php echo $data['vacation']?>" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Facebook</label>

                                <div class="col-sm-10">
                                    <input name="facebook" value="<?php echo $data['facebook']?>" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Twitter</label>

                                <div class="col-sm-10">
                                    <input name="twitter" value="<?php echo $data['twitter']?>" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Linkedin</label>

                                <div class="col-sm-10">
                                    <input name="linkedin" value="<?php echo $data['linkedin']?>" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Instragram</label>

                                <div class="col-sm-10">
                                    <input name="instragram" value="<?php echo $data['instragram']?>" type="text" class="form-control" >
                                </div>
                            </div> <div class="form-group">
                                <label  class="col-sm-2 control-label">Cart</label>

                                <div class="col-sm-10">
                                    <input name="cart" value="<?php echo $data['cart']?>" type="text" class="form-control" >
                                </div>
                            </div> <div class="form-group">
                                <label  class="col-sm-2 control-label">Download Resume</label>

                                <div class="col-sm-10">
                                    <input name="download" value="<?php echo $data['download']?>" type="text" class="form-control" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Image</label>
                                <div>

                                    <img width="100" src="view/admin/basic_info/uploads/<?php echo $basics['img']?>" alt="">
                                </div>

                               <div class="col-sm-10">
                                    <input name="img" type="file" class="form-control" >
                                </div>
                            </div>


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">

                            <button type="submit" class="btn btn-success">submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>

                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->


    </div>





<?php
include_once '../includes/footer.php';
?>