<?php
include_once '../includes/header.php';
?>



    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Blank page
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Blank page</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">INTERESTS</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="view/admin/interests/store.php" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">INTERESTS-1</label>

                                <div class="col-sm-10">
                                    <input name="interest1" type="text" class="form-control" >
                                </div>
                            </div>


                        </div>
                       <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">INTERESTS-1 Image</label>

                                <div class="col-sm-10">
                                    <input name="interest1_img" type="file" class="form-control" >
                                </div>
                            </div>


                        </div>
                       <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">INTERESTS-2 </label>

                                <div class="col-sm-10">
                                    <input name="interest2" type="text" class="form-control" >
                                </div>
                            </div>


                        </div> <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">INTERESTS-2 Image</label>

                                <div class="col-sm-10">
                                    <input name="interest2_img" type="file" class="form-control" >
                                </div>
                            </div>


                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">INTERESTS-3</label>

                                <div class="col-sm-10">
                                    <input name="interest3" type="text" class="form-control" >
                                </div>
                            </div>


                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">INTERESTS-3 image</label>

                                <div class="col-sm-10">
                                    <input name="interest3_img" type="file" class="form-control" >
                                </div>
                            </div>


                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">INTERESTS-4</label>

                                <div class="col-sm-10">
                                    <input name="interest4" type="text" class="form-control" >
                                </div>
                            </div>



                        </div> <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">INTERESTS-4 Image</label>

                                <div class="col-sm-10">
                                    <input name="interest4_img" type="file" class="form-control" >
                                </div>
                            </div>



                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">INTERESTS-5  </label>

                                <div class="col-sm-10">
                                    <input name="interest5" type="text" class="form-control" >
                                </div>
                            </div>



                        </div> <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">INTERESTS-5 Image</label>

                                <div class="col-sm-10">
                                    <input name="interest5_img" type="file" class="form-control" >
                                </div>
                            </div>



                        </div>

                        <!-- /.box-body -->
                        <div class="box-footer">

                            <button type="submit" class="btn btn-success">submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>

                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->


    </div>





<?php
include_once '../includes/footer.php';
?>