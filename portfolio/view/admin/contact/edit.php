<?php
include_once '../includes/header.php';
include_once '../../../vendor/autoload.php';
$contacts=new \App\admin\contactUs();
$contact=$contacts->show($_GET['id']);

?>



    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Blank page
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Blank page</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Contact Info From</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="view/admin/contact/update.php" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Email</label>

                                <div class="col-sm-10">
                                    <input name="email" value="<?php echo $contact['email'] ?>" type="email" class="form-control" >
                                    <input name="id"  value="<?php echo $contact['id'] ?>"  type="hidden" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Phone</label>

                                <div class="col-sm-10">
                                    <input name="phone" value="<?php echo $contact['phone'] ?>"  type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Viber</label>

                                <div class="col-sm-10">
                                    <input name="viber" value="<?php echo $contact['viber'] ?>"  type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Facebook</label>

                                <div class="col-sm-10">
                                    <input name="facebook" value="<?php echo $contact['facebook'] ?>" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Address</label>

                                <div class="col-sm-10">
                                    <input name="address" value="<?php echo $contact['address'] ?>" type="text" class="form-control" >
                                </div>
                            </div>




                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">

                            <button type="submit" class="btn btn-success">submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>

                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->


    </div>





<?php
include_once '../includes/footer.php';
?>