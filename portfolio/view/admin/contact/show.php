<?php
include_once '../includes/header.php';
include_once '../../../vendor/autoload.php';
$contact=new \App\admin\contactUs();
$contact=$contact->show($_GET['id']);
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">

            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"> Contact Info Table </h3>
                    <h3 class="box-title"><a href="view/admin/contact/pdf.php?id=<?php echo $contact['unique_id']?>">Pdf Download</a> </h3>

                </div>


                <section class="content">

                    <!-- Default box -->
                    <div class="box">
                        <div class="box-header with-border">
                            <label for="">Email</label>
                            <div>
                                <h3 class="box-title"><?php echo $contact['email']?></h3>
                            </div>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-header with-border">
                            <label for="">Phone</label>
                            <div>
                                <h3 class="box-title"><?php echo $contact['phone']?></h3>
                            </div>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div> <div class="box-header with-border">
                            <label for="">Viber</label>
                            <div>
                                <h3 class="box-title"><?php echo $contact['viber']?></h3>
                            </div>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div> <div class="box-header with-border">
                            <label for="">Facebook</label>
                            <div>
                                <h3 class="box-title"><?php echo $contact['facebook']?></h3>
                            </div>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div> <div class="box-header with-border">
                            <label for="">address</label>
                            <div>
                                <h3 class="box-title"><?php echo $contact['address']?></h3>
                            </div>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                        </div>


                        <!-- /.box-body -->

                    </div>
                    <!-- /.box -->

                </section>











<?php
include_once '../includes/footer.php';
?>
