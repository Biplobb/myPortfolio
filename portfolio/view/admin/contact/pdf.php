<?php
include_once '../../../vendor/autoload.php';
// reference the Dompdf namespace
use Dompdf\Dompdf;

$contacts=new \App\admin\contactUs();
$contact=$contacts->show($_GET['id']);
$email=$contact['email'];
$phone=$contact['phone'];
$viber=$contact['viber'];
$facebook=$contact['facebook'];
$address=$contact['address'];

$html="

 
<html>
<head>
    
    <title>Document</title>
    <style>
    td{padding: 10px}
</style>
</head>
<body>
<table border='1'>
    <tr>
        <td>Email</td>
        <td>Phone</td>
        <td>Viber</td>
        <td>Facebook</td>
        <td>Address</td>
       
    </tr>
    <tr>
        <td>$email</td>
        <td>$phone</td>
        <td>$viber</td>
        <td>$facebook</td>
        <td>$address</td>
         
    </tr>
</table>
</body>
</html>

";

$dompdf = new Dompdf();
$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'landscape');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream();
