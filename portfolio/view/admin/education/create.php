<?php
include_once '../includes/header.php';
?>



    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Blank page
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Blank page</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">

                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Basic Info From</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="view/admin/education/store.php" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">School</label>

                                <div class="col-sm-10">
                                    <input name="school" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">College</label>

                                <div class="col-sm-10">
                                    <input name="college" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">University</label>

                                <div class="col-sm-10">
                                    <input name="university" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label"> Other Course</label>

                                <div class="col-sm-10">
                                    <input name="other" type="text" class="form-control" >
                                </div>
                            </div>







                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">

                            <button type="submit" class="btn btn-success">submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>

                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->


    </div>





<?php
include_once '../includes/footer.php';
?>