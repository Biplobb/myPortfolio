<footer class="footer">
    <div class="footer-social">
        <ul class="social">
            <li><a href="https://www.twitter.com/" target="_blank"><i class="rsicon rsicon-twitter"></i></a></li>
            <li><a href="https://www.facebook.com/" target="_blank"><i class="rsicon rsicon-facebook"></i></a></li>
            <li><a href="https://dribbble.com/" target="_blank"><i class="rsicon rsicon-dribbble"></i></a></li>
            <li><a href="https://www.linkedin.com/" target="_blank"><i class="rsicon rsicon-linkedin"></i></a></li>
            <li><a href="https://instagram.com/" target="_blank"><i class="rsicon rsicon-instagram"></i></a></li>
            <li><a href="https://plus.google.com/" target="_blank"><i class="rsicon rsicon-google-plus"></i></a></li>
            <li><a href="http://xing.com/" target="_blank"><i class="rsicon rsicon-xing"></i></a></li>
        </ul>    </div>
</footer>
</div>

<a href="#" class="btn-scroll-top"><i class="rsicon rsicon-arrow-up"></i></a>
<div id="overlay"></div>
<div id="preloader">
    <div class="preload-icon"><span></span><span></span></div>
    <div class="preload-text"></div>
</div>


<script type='text/javascript' src='assets/front/material-11/www.google.com/recaptcha/apib429.js?onload=PxRecaptchaCallback&amp;render=explicit&amp;ver=4.7.6'></script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-content/plugins/rs-card-contact-form/js/contact-form.js?ver=4.7.6'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View Cart","cart_url":"http:\/\/rscardwp.px-lab.com\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
    /* ]]> */
</script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=2.6.9'></script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
    /* ]]> */
</script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=2.6.9'></script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-content/plugins/woocommerce/assets/js/jquery-cookie/jquery.cookie.min.js?ver=1.4.1'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","fragment_name":"wc_fragments"};
    /* ]]> */
</script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=2.6.9'></script>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=+AIzaSyA5OES_3FSQgjB8F_RCyJZnblbM4TReJXU&amp;ver=4.7.6'></script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-content/themes/rs-card/fonts/map-icons/js/map-icons.min.js?ver=4.7.6'></script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-includes/js/imagesloaded.min.js?ver=3.2.0'></script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-content/themes/rs-card/js/plugins/isotope.pkgd.min.js?ver=4.7.6'></script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-content/themes/rs-card/js/plugins/jquery.appear.js?ver=4.7.6'></script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-content/themes/rs-card/js/plugins/jquery.onepagenav.min.js?ver=4.7.6'></script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-content/themes/rs-card/js/plugins/jquery.bxslider/jquery.bxslider.min.js?ver=4.7.6'></script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-content/themes/rs-card/js/plugins/jquery.customscroll/jquery.mCustomScrollbar.concat.min.js?ver=4.7.6'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var mejsL10n = {"language":"en-US","strings":{"Close":"Close","Fullscreen":"Fullscreen","Turn off Fullscreen":"Turn off Fullscreen","Go Fullscreen":"Go Fullscreen","Download File":"Download File","Download Video":"Download Video","Play":"Play","Pause":"Pause","Captions\/Subtitles":"Captions\/Subtitles","None":"None","Time Slider":"Time Slider","Skip back %1 seconds":"Skip back %1 seconds","Video Player":"Video Player","Audio Player":"Audio Player","Volume Slider":"Volume Slider","Mute Toggle":"Mute Toggle","Unmute":"Unmute","Mute":"Mute","Use Up\/Down Arrow keys to increase or decrease volume.":"Use Up\/Down Arrow keys to increase or decrease volume.","Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.":"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds."}};
    var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/"};
    /* ]]> */
</script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=2.22.0'></script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-content/themes/rs-card/js/plugins/jquery.owlcarousel/owl.carousel.js?ver=4.7.6'></script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-content/themes/rs-card/js/plugins/jquery.slick/slick.min.js?ver=4.7.6'></script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-includes/js/wp-embed.min.js?ver=4.7.6'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var icl_vars = {"current_language":"en","icl_home":"http:\/\/rscardwp.px-lab.com","ajax_url":"http:\/\/rscardwp.px-lab.com\/wp-admin\/admin-ajax.php","url_type":"3"};
    /* ]]> */
</script>
<script type='text/javascript' src='http://rscardwp.px-lab.com/wp-content/plugins/sitepress-multilingual-cms/res/js/sitepress.js?ver=4.7.6'></script>
</body>

<!-- Mirrored from rscardwp.px-lab.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 31 Oct 2017 08:19:20 GMT -->
</html>
