
<!DOCTYPE html>
<html lang="en-US" class="theme-color-e86767 light_skin">

<!-- Mirrored from rscardwp.px-lab.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 31 Oct 2017 08:19:18 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="pingback" href="http://rscardwp.px-lab.com/xmlrpc.php">

    <link rel="icon" href="http://rscardwp.px-lab.com/wp-content/uploads/2016/01/favicon.png">
    <title>Rs-Card &#8211; Just another WordPress site</title>

    <link rel="stylesheet" href="http://rscardwp.px-lab.com/wp-content/plugins/sitepress-multilingual-cms/res/css/language-selector.css?v=3.4.1" type="text/css" media="all" />
    <meta name='robots' content='noindex,follow' />
    <link rel="alternate" hreflang="en" href="index.php" />
    <link rel="alternate" hreflang="de" href="http://rscardwp.px-lab.com/?lang=de" />
    <link rel='dns-prefetch' href='http://www.google.com/' />
    <link rel='dns-prefetch' href='http://maps.googleapis.com/' />
    <link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
    <link rel='dns-prefetch' href='http://s.w.org/' />
    <link rel="alternate" type="application/rss+xml" title="Rs-Card &raquo; Feed" href="http://rscardwp.px-lab.com/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Rs-Card &raquo; Comments Feed" href="http://rscardwp.px-lab.com/comments/feed/" />
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/rscardwp.px-lab.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7.6"}};
        !function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='rscard-woocommerce-css'  href='http://rscardwp.px-lab.com/wp-content/themes/rs-card/woocommerce/css/woocommerce.css?ver=4.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='rscard-woocommerce-layout-css'  href='http://rscardwp.px-lab.com/wp-content/themes/rs-card/woocommerce/css/woocommerce-layout.css?ver=4.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='rscard-woocommerce-smallscreen-css'  href='http://rscardwp.px-lab.com/wp-content/themes/rs-card/woocommerce/css/woocommerce-smallscreen.css?ver=4.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='google-font0-css'  href='https://fonts.googleapis.com/css?family=Fredoka+One&amp;ver=4.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='Open Sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700&amp;ver=4.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='Fredoka One-css'  href='https://fonts.googleapis.com/css?family=Fredoka+One%3A400&amp;ver=4.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='icon-fonts-map-css'  href='http://rscardwp.px-lab.com/wp-content/themes/rs-card/fonts/map-icons/css/map-icons.min.css?ver=4.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='icon-fonts-css'  href='http://rscardwp.px-lab.com/wp-content/themes/rs-card/fonts/icomoon/style.css?ver=4.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='map-icons-css'  href='http://rscardwp.px-lab.com/wp-content/themes/rs-card/fonts/map-icons/css/map-icons.min.css?ver=4.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='icomoon-css'  href='http://rscardwp.px-lab.com/wp-content/themes/rs-card/fonts/icomoon/style.css?ver=4.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='bxslider-css'  href='http://rscardwp.px-lab.com/wp-content/themes/rs-card/js/plugins/jquery.bxslider/jquery.bxslider.css?ver=4.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='custom-scroll-css'  href='http://rscardwp.px-lab.com/wp-content/themes/rs-card/js/plugins/jquery.customscroll/jquery.mCustomScrollbar.min.css?ver=4.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='mediaelement-player-css'  href='http://rscardwp.px-lab.com/wp-content/themes/rs-card/js/plugins/jquery.mediaelement/mediaelementplayer.min.css?ver=4.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='owl-carousel-css'  href='http://rscardwp.px-lab.com/wp-content/themes/rs-card/js/plugins/jquery.owlcarousel/owl.carousel.css?ver=4.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='owl-carousel-theme-css'  href='http://rscardwp.px-lab.com/wp-content/themes/rs-card/js/plugins/jquery.owlcarousel/owl.theme.css?ver=4.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='slick-css'  href='http://rscardwp.px-lab.com/wp-content/themes/rs-card/js/plugins/jquery.slick/slick.css?ver=4.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='rscard-style-css'  href='http://rscardwp.px-lab.com/wp-content/themes/rs-card/style.css?ver=4.7.6' type='text/css' media='all' />
    <link rel='stylesheet' id='theme-color-css'  href='http://rscardwp.px-lab.com/wp-content/themes/rs-card/colors/theme-color.css?ver=4.7.6' type='text/css' media='all' />
    <script type='text/javascript' src='http://rscardwp.px-lab.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
    <script type='text/javascript' src='http://rscardwp.px-lab.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var ajax_var = {"url":"http:\/\/rscardwp.px-lab.com\/wp-admin\/admin-ajax.php","nonce":"79e5611943"};
        var date = {"months":["January","March","March","May","May","July","July","August","October","October","December","December"],"weeks":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]};
        /* ]]> */
    </script>
    <script type='text/javascript' src='http://rscardwp.px-lab.com/wp-content/themes/rs-card/js/site.js?ver=1'></script>
    <script type='text/javascript' src='http://rscardwp.px-lab.com/wp-content/themes/rs-card/js/libs/modernizr.js?ver=4.7.6'></script>
    <link rel='https://api.w.org/' href='http://rscardwp.px-lab.com/wp-json/' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://rscardwp.px-lab.com/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://rscardwp.px-lab.com/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 4.7.6" />
    <meta name="generator" content="WooCommerce 2.6.9" />
    <link rel="canonical" href="index.php" />
    <link rel='shortlink' href='index.php' />
    <link rel="alternate" type="application/json+oembed" href="http://rscardwp.px-lab.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Frscardwp.px-lab.com%2F" />
    <link rel="alternate" type="text/xml+oembed" href="http://rscardwp.px-lab.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Frscardwp.px-lab.com%2F&amp;format=xml" />
    <meta name="generator" content="WPML ver:3.4.1 stt:1,3;" />
    <!--[if lt IE 9]><script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script><script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]--><style type='text/css'>
        body,
        select,
        textarea,
        input[type='tel'],
        input[type='text'],
        input[type='email'],
        input[type='search'],
        input[type='password'],
        .btn,
        .filter button,
        .nav-wrap .nav a,
        .mobile-nav .nav a {
            font-family: "Open Sans";
        }

        .logo,
        .site-title {
            font-family: "Fredoka One";
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6  {
            font-family: "Open Sans";
        }
        .head-bg:before {
            background-color: rgba(0,0,0,0.8);
        }
        .theme-color-e86767 a,
        .theme-color-e86767 blockquote:before,
        .theme-color-e86767 .contact-map .contact-info a:hover,
        .theme-color-e86767 .interests-list i,
        .theme-color-e86767 .input-field.used label,
        .theme-color-e86767 .logo span,
        .theme-color-e86767 #map .map-icon,
        .theme-color-e86767 .head-cont .btn-mobile,
        .theme-color-e86767 .page-404 h2 span,
        .theme-color-e86767 .post-box .post-title a:hover,
        .theme-color-e86767 .post-single .post-title a:hover,
        .theme-color-e86767 .post-pagination .post-title a:hover,
        .theme-color-e86767 .post-comments .section-title,
        .theme-color-e86767 .ref-box .person-speech:before,
        .theme-color-e86767 .service-icon,
        .theme-color-e86767 .statistic-value,
        .theme-color-e86767 .service-sub-title,
        .theme-color-e86767 .styled-list li:before,
        .theme-color-e86767 .timeline-box .date,
        .theme-color-e86767 .twitter-icon .rsicon,
        .theme-color-e86767 .tabs-vertical .tabs-menu a:hover,
        .theme-color-e86767 .tabs-vertical .tabs-menu .active a,
        .theme-color-e86767 .widget-title,
        .theme-color-e86767 .widget_search label:before,
        .theme-color-e86767 .widget_search .search-form:before,
        .theme-color-e86767 .widget_meta ul li a:hover,
        .theme-color-e86767 .widget_archive ul li a:hover,
        .theme-color-e86767 .widget_nav_menu ul li a:hover,
        .theme-color-e86767 .widget_categories ul li a:hover,
        .theme-color-e86767 .widget_recent_entries ul li a:hover,
        .theme-color-e86767 .widget_recent_comments ul li a:hover,
        .theme-color-e86767 .widget-popuplar-posts .post-title a:hover,
        .theme-color-e86767 .widget-recent-posts .post-title a:hover,
        .theme-color-e86767 .head-woo-count {
            color: #e86767; }

        .theme-color-e86767 .head-nav .sub-menu li:hover>a,
        .theme-color-e86767 .head-nav .sub-menu li.active,
        .theme-color-e86767 .head-lang .lang-list a:hover{
            color: #e86767 !important;
        }

        .theme-color-e86767 mark,
        .theme-color-e86767 .btn-primary,
        .theme-color-e86767 .btn-primary-outer,
        .theme-color-e86767 .btn-sidebar-close,
        .theme-color-e86767 .calendar-today .date,
        .theme-color-e86767 .calendar-body .busy-day,
        .theme-color-e86767 .calendar-body td .current-day,
        .theme-color-e86767 .filter .active:after,
        .theme-color-e86767 .filter-bar .filter-bar-line,
        .theme-color-e86767 .input-field .line:before,
        .theme-color-e86767 .input-field .line:after,
        .theme-color-e86767 .mobile-nav,
        .theme-color-e86767 .head-nav .nav>ul>li>a:after,
        .theme-color-e86767 .post-datetime,
        .theme-color-e86767 .profile-social,
        .theme-color-e86767 .profile-preword span,
        .theme-color-e86767 .progress-bar .bar-fill,
        .theme-color-e86767 .progress-bar .bar-line:after,
        .theme-color-e86767 .price-box.box-primary .btn,
        .theme-color-e86767 .price-box.box-primary .price-box-top,
        .theme-color-e86767 .profile-list .button,

        .theme-color-e86767 .pagination span.page-numbers.current,
        .theme-color-e86767 .pagination a.page-numbers:active,

        .theme-color-e86767 .latest-tweets .slick-dots button:hover,
        .theme-color-e86767 .latest-tweets .slick-dots .slick-active button,

        .theme-color-e86767 .tabs-horizontal .tabs-menu a:hover:after,
        .theme-color-e86767 .tabs-horizontal .tabs-menu .active a:after,
        .theme-color-e86767 .togglebox-header,
        .theme-color-e86767 .accordion-header,
        .theme-color-e86767 .timeline-bar,
        .theme-color-e86767 .timeline-box .dot,
        .theme-color-e86767 .timeline-box-compact .date span,
        .theme-color-e86767 .widget_tag_cloud a:hover,
        .theme-color-e86767 .widget_product_tag_cloud a:hover,
        .theme-color-e86767 .wpcf7-form .wpcf7-submit {
            background-color: #e86767; }

        .theme-color-e86767 .mejs-container .mejs-controls .mejs-time-rail .mejs-time-current,
        .theme-color-e86767 .mejs-container .mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-current {
            background: #e86767; }

        .theme-color-e86767 .timeline-box-inner,
        .theme-color-e86767 .price-box.box-primary .btn,
        .theme-color-e86767 .widget_search .search-form,
        .theme-color-e86767 .widget_product_search .search-form,
        .theme-color-e86767 .widget_tag_cloud a:hover,
        .theme-color-e86767 .widget_product_tag_cloud a:hover,
        .theme-color-e86767 .wpcf7-form .wpcf7-form-control:focus {
            border-color: #e86767; }

        .theme-color-e86767 .page-404 h2 span:before,
        .theme-color-e86767 .profile-preword span:before,
        .theme-color-e86767 .timeline-box-compact .date span:before {
            border-left-color: #e86767; }

        .theme-color-e86767 .price-box.box-primary .price-box-top:before {
            border-top-color: #e86767; }
        .woocommerce .star-rating,
        .woocommerce .star-rating:before,
        .woocommerce .product-links .button,
        .woocommerce .product-links .button:hover,
        .woocommerce div.product p.price,
        .woocommerce div.product span.price,
        .widget_product_search .woocommerce-product-search:before {
            color: #e86767;
        }

        .woocommerce span.onsale,
        .woocommerce #respond input#submit.alt,
        .woocommerce a.button.alt,
        .woocommerce button.button.alt,
        .woocommerce input.button.alt,
        .woocommerce div.product .woocommerce-tabs ul.tabs li a:hover:after,
        .woocommerce div.product .woocommerce-tabs ul.tabs li.active a:after {
            background-color: #e86767;
        }

        .woocommerce span.onsale:before {
            border-left-color: #e86767;
        }

        .widget_product_search .woocommerce-product-search {
            border-color: #e86767;
        }</style></head>