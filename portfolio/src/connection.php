<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/2/2017
 * Time: 3:31 AM
 */
namespace App;
use PDO;
use PDOException;
class connection
{
    protected $conn;
    private $user = 'root';
    private $pass = '';
    public function __construct(){
        try {
            $this->conn = new PDO('mysql:host=localhost;dbname=portfolio', $this->user, $this->pass);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);



        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

}