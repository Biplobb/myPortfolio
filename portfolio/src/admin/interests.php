<?php

namespace App\admin;


use App\connection;
use PDO;
use PDOException;

class interests extends connection
{
    private $id;
    private $interest1;
    private $interest1_img;
    private $interest2;
    private $interest2_img;
    private $interest3;
    private $interest3_img;
    private $interest4;
    private $interest4_img;
    private $interest5;
    private $interest5_img;

    public function set($data = array())
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('interest1', $data)) {
            $this->interest1 = $data['interest1'];
        } if (array_key_exists('interest1_img', $data)) {
            $this->interest1_img = $data['interest1_img'];
        }
        if (array_key_exists('interest2', $data)) {
            $this->interest2 = $data['interest2'];
        }if (array_key_exists('interest2_img', $data)) {
            $this->interest2_img = $data['interest2_img'];
        }
        if (array_key_exists('interest3', $data)) {
            $this->interest3 = $data['interest3'];
        } if (array_key_exists('interest3_img', $data)) {
            $this->interest3_img = $data['interest3_img'];
        }
        if (array_key_exists('interest4', $data)) {
            $this->interest4 = $data['interest4'];
        } if (array_key_exists('interest4_img', $data)) {
            $this->interest4_img = $data['interest4_img'];
        }
        if (array_key_exists('interest5', $data)) {
            $this->interest5 = $data['interest5'];
        } if (array_key_exists('interest5_img', $data)) {
            $this->interest5_img = $data['interest5_img'];
        }
        return $this;
    }
    public function store(){
        try {

            $stmt =  $this->conn->prepare("INSERT INTO `interests` (`interest1`,`interest1_img`,`interest2`,`interest2_img`,`interest3`,`interest3_img`,`interest4`,`interest4_img`,`interest5`,`interest5_img`,`unique_id`) VALUES
 
 
 (:interest1,:interest1_img,:interest2,:interest2_img,:interest3,:interest3_img,:interest4,:interest4_img,:interest5,:interest5_img,:unique_id)");

            $stmt->bindValue(':interest1', $this->interest1 , PDO::PARAM_STR);
            $stmt->bindValue(':interest1_img', $this->interest1_img , PDO::PARAM_STR);
            $stmt->bindValue(':interest2', $this->interest2 , PDO::PARAM_STR);
            $stmt->bindValue(':interest2_img', $this->interest2_img , PDO::PARAM_STR);
            $stmt->bindValue(':interest3', $this->interest3 , PDO::PARAM_STR);
            $stmt->bindValue(':interest3_img', $this->interest3_img , PDO::PARAM_STR);
            $stmt->bindValue(':interest4', $this->interest4 , PDO::PARAM_STR);
            $stmt->bindValue(':interest4_img', $this->interest4_img , PDO::PARAM_STR);
            $stmt->bindValue(':interest5', $this->interest5 , PDO::PARAM_STR);
            $stmt->bindValue(':interest5_img', $this->interest5_img , PDO::PARAM_STR);
            $stmt->bindValue(':unique_id', md5(time()) , PDO::PARAM_STR);



            $stmt->execute();



        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function upload_img1(){
        $img_name1=$_FILES['interest1_img']['name'];
        $tmp_name1=$_FILES['interest1_img']['tmp_name'];
        $new_name1=substr(md5(time()),'0','8');
        $ex_name1=explode('.',$img_name1);
        $name1=end($ex_name1);
        $_POST['interest1_img']=$new_name1.'.'.$name1;
        move_uploaded_file($tmp_name1, '../../../view/admin/interests/uploads/uploads1/'.$_POST['interest1_img']);
        return $_POST['interest1_img'];
    }
    public function upload_img2(){
        $img_name2=$_FILES['interest2_img']['name'];
        $tmp_name2=$_FILES['interest2_img']['tmp_name'];
        $new_name2=substr(md5(time()),'0','8');
        $ex_name2=explode('.',$img_name2);
        $name2=end($ex_name2);
        $_POST['interest2_img']=$new_name2.'.'.$name2;
        move_uploaded_file($tmp_name2, '../../../view/admin/interests/uploads/uploads2/'.$_POST['interest2_img']);
        return $_POST['interest2_img'];
    }
    public function upload_img3(){
        $img_name3=$_FILES['interest3_img']['name'];
        $tmp_name3=$_FILES['interest3_img']['tmp_name'];
        $new_name3=substr(md5(time()),'0','8');
        $ex_name3=explode('.',$img_name3);
        $name3=end($ex_name3);
        $_POST['interest3_img']=$new_name3.'.'.$name3;
        move_uploaded_file($tmp_name3, '../../../view/admin/interests/uploads/uploads3/'.$_POST['interest3_img']);
        return $_POST['interest3_img'];
    }
    public function upload_img4(){
        $img_name4=$_FILES['interest4_img']['name'];
        $tmp_name4=$_FILES['interest4_img']['tmp_name'];
        $new_name4=substr(md5(time()),'0','8');
        $ex_name4=explode('.',$img_name4);
        $name4=end($ex_name4);
        $_POST['interest4_img']=$new_name4.'.'.$name4;
        move_uploaded_file($tmp_name4, '../../../view/admin/interests/uploads/uploads4/'.$_POST['interest4_img']);
        return $_POST['interest4_img'];
    }
    public function upload_img5(){
        $img_name5=$_FILES['interest5_img']['name'];
        $tmp_name5=$_FILES['interest5_img']['tmp_name'];
        $new_name5=substr(md5(time()),'0','8');
        $ex_name5=explode('.',$img_name5);
        $name5=end($ex_name5);
        $_POST['interest5_img']=$new_name5.'.'.$name5;
        move_uploaded_file($tmp_name5, '../../../view/admin/interests/uploads/uploads5/'.$_POST['interest5_img']);
        return $_POST['interest5_img'];
    }
    public function index()
    {
        try {

            $stmt = $this->conn->prepare("SELECT * FROM `interests`");

            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        header('location:index.php');
    }
}

