<?php

namespace App\admin;


use App\connection;
use PDO;
use PDOException;

class education extends connection
{
    private $id;
    private $school;
    private $college;
    private $university;
    private $other;
    public function set($data = array())
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('school', $data)) {
            $this->school = $data['school'];
        }
        if (array_key_exists('college', $data)) {
            $this->college = $data['college'];
        }
        if (array_key_exists('university', $data)) {
            $this->university = $data['university'];
        }
        if (array_key_exists('other', $data)) {
            $this->other = $data['other'];
        }
        return $this;
    }
    public function store(){
        try {

            $stmt =  $this->conn->prepare("INSERT INTO `education` (`school`,`college`,`university`,`other`,`unique_id`) VALUES
 
 
 (:school,:college,:university,:other,:unique_id)");

            $stmt->bindValue(':school', $this->school , PDO::PARAM_STR);
            $stmt->bindValue(':college', $this->college , PDO::PARAM_STR);
            $stmt->bindValue(':university', $this->university , PDO::PARAM_STR);
            $stmt->bindValue(':other', $this->other , PDO::PARAM_STR);
            $stmt->bindValue(':unique_id', md5(time()) , PDO::PARAM_STR);



            $stmt->execute();



        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function index()
    {
        try {

            $stmt = $this->conn->prepare("SELECT * FROM `education`");

            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        header('location:index.php');
    }
}

