<?php

namespace App\admin\Auth;
use App\Connection;
use PDO;
use PDOException;
class Auth extends Connection
{

    private $name;
    private $email;
    private $password;

    public function set($data = array()){
        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        } if(array_key_exists('email',$data)){
            $this->email = $data['email'];
        }
        if(array_key_exists('password',$data)){
            $this->password = $data['password'];
        }

        return $this;
    }




    public function login(){
        try {

            $stm =  $this->conn->prepare("SELECT * FROM `basic_info` where (email = :email Or name = :name) AND password = :password");
            $stm->bindValue(':name', $this->name, PDO::PARAM_STR);
            $stm->bindValue(':email', $this->email, PDO::PARAM_STR);
            $stm->bindValue(':password', $this->password, PDO::PARAM_STR);
            $stm->execute();
            return $stm->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}