<?php

include_once 'view/front/includes/header.php';
include_once 'vendor/autoload.php';
$basic_info=new \App\admin\basicInfo();
$info=$basic_info->view();

$contact=new \App\admin\contactUs();
$contacts=$contact->view();

$education=new \App\admin\education();
$educations=$education->index();

$experience=new \App\admin\experience();
$experience=$experience->index();

$service=new \App\admin\service();
$service=$service->index();

$interests=new \App\admin\interests();
$interests=$interests->index();

$skill=new \App\admin\skill();
$skill=$skill->index();

?>

<body class="home page-template page-template-page-home page-template-page-home-php page page-id-310 header-has-img  masthead-fixed loading">

<div class="mobile-nav">
    <button class="btn-mobile mobile-nav-close"><i class="rsicon rsicon-close"></i></button>
    <div id="mobile-nav" class="mobile-nav-inner">
        <nav class="nav"><ul class="clearfix"><li id="menu-item-300" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-300"><a href="index.php#about">About</a></li>
                <li id="menu-item-301" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home current-menu-ancestor current-menu-parent menu-item-has-children menu-item-301"><a href="index.php#skills">Skills</a>
                    <ul class="sub-menu">
                        <li id="menu-item-303" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-303"><a href="index.php#portfolio">Portfolio</a></li>
                    </ul>
                </li>
                <li id="menu-item-397" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home current-menu-ancestor current-menu-parent menu-item-has-children menu-item-397"><a href="index.php#experience">experience</a>
                    <ul class="sub-menu">
                        <li id="menu-item-399" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-399"><a href="index.php#education">Education</a></li>
                        <li id="menu-item-426" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-426"><a href="index.php#clients">Clients</a></li>
                        <li id="menu-item-427" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-427"><a href="index.php#references">References</a></li>
                    </ul>
                </li>
                <li id="menu-item-428" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-has-children menu-item-428"><a href="index.php#pricing">Pricing</a>
                    <ul class="sub-menu">
                        <li id="menu-item-539" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-539"><a href="http://rscardwp.px-lab.com/shop/">Shop</a></li>
                    </ul>
                </li>
                <li id="menu-item-313" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-313"><a href="http://rscardwp.px-lab.com/blog/">Blog</a>
                    <ul class="sub-menu">
                        <li id="menu-item-316" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-316"><a href="http://rscardwp.px-lab.com/how-ipsum-project-web-dolor-sit-amet/">Image Post</a></li>
                        <li id="menu-item-315" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-315"><a href="http://rscardwp.px-lab.com/ipsum-project-web/">Slider Post</a></li>
                        <li id="menu-item-314" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-314"><a href="http://rscardwp.px-lab.com/how-ipsum-project-web-dolor-sit-amet-2/">Video Post</a></li>
                        <li id="menu-item-317" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-317"><a href="http://rscardwp.px-lab.com/web-dolor-sit-amet/">Audio Post</a></li>
                        <li id="menu-item-318" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-318"><a href="http://rscardwp.px-lab.com/how-ipsum-project-web/">Vimeo Post</a></li>
                        <li id="menu-item-319" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-319"><a href="http://rscardwp.px-lab.com/how-ipsum-project/">Youtube Post</a></li>
                        <li id="menu-item-320" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-320"><a href="http://rscardwp.px-lab.com/disneys-alice-in-wonderland/">Dailymotion Post</a></li>
                        <li id="menu-item-311" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-311"><a href="http://rscardwp.px-lab.com/the-history-and-future-of-motorcycles/">Without Media Post</a></li>
                    </ul>
                </li>
                <li id="menu-item-306" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-306"><a href="index.php#calendar">Calendar</a></li>
                <li id="menu-item-307" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-307"><a href="index.php#contact">Contact</a></li>
                <li id="menu-item-425" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-425"><a href="#">Other</a>
                    <ul class="sub-menu">
                        <li id="menu-item-312" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-312"><a href="http://rscardwp.px-lab.com/typography-page/">Typography Page</a></li>
                        <li id="menu-item-308" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-308"><a href="http://rscardwp.px-lab.com/404">404 Page</a></li>
                    </ul>
                </li>
                <li id="menu-item-560" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-560"><a href="https://px-lab.com/rscard-previews/">All Demos</a>
                    <ul class="sub-menu">
                        <li id="menu-item-561" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-561"><a href="https://themeforest.net/item/resume-cv-vcard-theme/14502131-lab?ref=PX-lab">Buy WordPress version</a></li>
                    </ul>
                </li>
            </ul></nav>  </div>
</div>

<div class="sidebar sidebar-fixed">
    <button class="btn-sidebar btn-sidebar-close"><img src="assets/admin/picture/contact.jpg"  alt=""></button>
    <div class="widget-area">
        <aside class="widget widget-profile">
            <div class="profile-photo">
                <img src="view/admin/basic_info/uploads/<?php echo $info['img']?>" alt="Author Image">
            </div>
            <div class="profile-info">
            </div>
        </aside>
        <aside class="widget rs-card-contact-form"><h2 class="widget-title">Contact me</h2>            <form class='contactForm' action='#' method='post'>
                <div class='input-field'>
                    <input class='contact-name' type='text' name='name'/>
                    <span class='line'></span>
                    <label>Name</label>
                </div>

                <div class='input-field'>
                    <input class='contact-email' type='email' name='email'/>
                    <span class='line'></span>
                    <label>Email</label>
                </div>

                <div class='input-field'>
                    <input class='contact-subject' type='text' name='subject'/>
                    <span class='line'></span>
                    <label>Subject</label>
                </div>

                <div class='input-field'>
                    <textarea class='contact-message' rows='4' name='message'></textarea>
                    <span class='line'></span>
                    <label>Message</label>
                </div>
                <input type='hidden' class='email_to' name='email_to' value='support@px-lab.com'/>
                <div class="g-recaptcha" data-theme="light" data-sitekey="6LcQihQUAAAAAMsDTvWEE9-d79x3A_TOpu0kWUAt" id='recaptcha2'></div>
                <span class='btn-outer btn-primary-outer ripple'>
					<input class='contact-submit btn btn-lg btn-primary' type='submit' value='Send'/>
				</span>

                <div class='contact-response'></div>
            </form>
        </aside><aside class="widget widget_search">
            <form method="get" class="search-form" action="http://rscardwp.px-lab.com/">
                <label>
                    <span class="screen-reader-text">Search for</span>
                    <input type="search"  placeholder="Search ..." value="" name="s" title="Search for:">
                </label>
                <input type="submit" class="search-submit" value="Search">
                <input type='hidden' name='lang' value='en' /></form></aside><aside class="widget widget-popuplar-posts"><h2 class="widget-title">Popular Posts</h2>            <ul>
                <li>
                    <div class="post-media"><a href="http://rscardwp.px-lab.com/disneys-alice-in-wonderland/"><img src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/thumb-449x286-3-1.jpg" alt=""/></a></div>
                    <div class="post-tag">
                        <a href="http://rscardwp.px-lab.com/category/uncategorized/">#Uncategorized</a>
                    </div>
                    <h3 class="post-title"><a href="http://rscardwp.px-lab.com/disneys-alice-in-wonderland/">DISNEY&#8217;S ALICE IN WONDERLAND</a></h3>
                    <div class="post-info">
                        <a href="http://rscardwp.px-lab.com/disneys-alice-in-wonderland/#respond"><i class="rsicon rsicon-comments"></i>0 comments</a>
                    </div>
                </li>
                <li>
                    <div class="post-media"><a href="http://rscardwp.px-lab.com/ipsum-project-web/"><img src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/rs-thumb-610x396-1.jpg" alt=""/></a></div>
                    <div class="post-tag">
                        <a href="http://rscardwp.px-lab.com/category/news/">#News</a>
                    </div>
                    <h3 class="post-title"><a href="http://rscardwp.px-lab.com/ipsum-project-web/">IPSUM PROJECT WEB</a></h3>
                    <div class="post-info">
                        <a href="http://rscardwp.px-lab.com/ipsum-project-web/#respond"><i class="rsicon rsicon-comments"></i>0 comments</a>
                    </div>
                </li>
                <li>
                    <div class="post-media"><a href="http://rscardwp.px-lab.com/web-dolor-sit-amet/"><img src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/photo-1445295029071-5151176738d0-1-78x56.jpg" alt=""/></a></div>
                    <div class="post-tag">
                        <a href="http://rscardwp.px-lab.com/category/news/">#News</a>
                    </div>
                    <h3 class="post-title"><a href="http://rscardwp.px-lab.com/web-dolor-sit-amet/">GIRL DOLOR SIT AMET</a></h3>
                    <div class="post-info">
                        <a href="http://rscardwp.px-lab.com/web-dolor-sit-amet/#respond"><i class="rsicon rsicon-comments"></i>0 comments</a>
                    </div>
                </li>
            </ul>
        </aside><aside class="widget widget_tag_cloud"><h2 class="widget-title">Tag Cloud</h2><div class="tagcloud"><a href='http://rscardwp.px-lab.com/tag/business/' class='tag-link-3 tag-link-position-1' title='3 topics' style='font-size: 8pt;'>business</a>
                <a href='http://rscardwp.px-lab.com/tag/city/' class='tag-link-4 tag-link-position-2' title='5 topics' style='font-size: 18.08pt;'>city</a>
                <a href='http://rscardwp.px-lab.com/tag/creative/' class='tag-link-5 tag-link-position-3' title='6 topics' style='font-size: 22pt;'>creative</a>
                <a href='http://rscardwp.px-lab.com/tag/fashion/' class='tag-link-6 tag-link-position-4' title='3 topics' style='font-size: 8pt;'>fashion</a>
                <a href='http://rscardwp.px-lab.com/tag/music/' class='tag-link-7 tag-link-position-5' title='3 topics' style='font-size: 8pt;'>music</a>
                <a href='http://rscardwp.px-lab.com/tag/news/' class='tag-link-8 tag-link-position-6' title='3 topics' style='font-size: 8pt;'>news</a>
                <a href='http://rscardwp.px-lab.com/tag/peoples/' class='tag-link-9 tag-link-position-7' title='4 topics' style='font-size: 13.6pt;'>peoples</a></div>
        </aside>		<aside class="widget widget_recent_entries">		<h2 class="widget-title">Recent posts</h2>		<ul>
                <li>
                    <a href="http://rscardwp.px-lab.com/disneys-alice-in-wonderland/">DISNEY&#8217;S ALICE IN WONDERLAND</a>
                </li>
                <li>
                    <a href="http://rscardwp.px-lab.com/ipsum-project-web/">IPSUM PROJECT WEB</a>
                </li>
                <li>
                    <a href="http://rscardwp.px-lab.com/web-dolor-sit-amet/">GIRL DOLOR SIT AMET</a>
                </li>
                <li>
                    <a href="http://rscardwp.px-lab.com/how-ipsum-project-web/">TRAVEL IPSUM PROJECT WEB</a>
                </li>
                <li>
                    <a href="http://rscardwp.px-lab.com/how-ipsum-project/">HOW IPSUM PROJECT</a>
                </li>
            </ul>
        </aside>		<aside class="widget widget_meta"><h2 class="widget-title">Meta</h2>			<ul>
                <li><a href="http://rscardwp.px-lab.com/wp-login.php">Log in</a></li>
                <li><a href="http://rscardwp.px-lab.com/feed/">Entries <abbr title="Really Simple Syndication">RSS</abbr></a></li>
                <li><a href="http://rscardwp.px-lab.com/comments/feed/">Comments <abbr title="Really Simple Syndication">RSS</abbr></a></li>
                <li><a href="https://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress.org</a></li>			</ul>
        </aside>    </div>
</div>
<div class="wrapper">
    <header class="header">
        <div class="head-bg" style="background-image: url('http://rscardwp.px-lab.com/wp-content/uploads/2016/02/rs-cover.jpg')"></div>

        <div class="head-bar has-woo has-lang has-sidebar">
            <div class="head-bar-inner">
                <div class="row">
                    <div class="col-lg-2 col-md-3 col-xs-6">
                        <a class="logo" href="index.php">
                            <span>MY</span>PORTFOLIO                            </a>
                    </div>

                    <div class="col-lg-10 col-md-9 col-xs-6">
                        <div class="head-cont clearfix">
                            <div class="head-items">
                                <a class="head-woo" href="<?php echo $info['cart']?>">
                                    <img src="assets/admin/picture/cart.jpg"  alt="">
                                    <span class="head-woo-count">0</span>
                                </a>

                                <div class="head-lang">
                                    <span class="lang-active">en</span>
                                    <ul class="lang-list">
                                        <li>
                                            <span class="icl_lang_sel_current icl_lang_sel_native">English</span>					</li>
                                        <li>
                                            <a href="http://rscardwp.px-lab.com/?lang=de">
                                            </a>
                                            <a href="http://rscardwp.px-lab.com/?lang=de">
                                                <span class="icl_lang_sel_native">Deutsch</span> <span class="icl_lang_sel_translated"><span class="icl_lang_sel_bracket">(</span>Bangladesh<span class="icl_lang_sel_bracket">)</span></span>							</a>
                                        </li>
                                    </ul>
                                </div>

                                <button class="btn-sidebar btn-sidebar-open"><img
                                        src="assets/admin/picture/contactss%20-%20Copy.jpg" alt=""></button>
                            </div>
                            <button class="btn-mobile btn-mobile-nav">Menu</button>
                            <div id="head-nav" class="head-nav">
                                <nav class="nav"><ul class="clearfix"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-300"><a href="index.php#about">About</a></li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home current-menu-ancestor current-menu-parent menu-item-has-children menu-item-301"><a href="index.php#skills">Skills</a>
                                            <ul class="sub-menu">
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-303"><a href="index.php#portfolio">Portfolio</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home current-menu-ancestor current-menu-parent menu-item-has-children menu-item-397"><a href="index.php#experience">experience</a>
                                            <ul class="sub-menu">
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-399"><a href="index.php#education">Education</a></li>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-426"><a href="index.php#clients">Clients</a></li>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-427"><a href="index.php#references">References</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-has-children menu-item-428"><a href="index.php#pricing">Pricing</a>
                                            <ul class="sub-menu">
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-539"><a href="http://rscardwp.px-lab.com/shop/">Shop</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-313"><a href="http://rscardwp.px-lab.com/blog/">Blog</a>
                                            <ul class="sub-menu">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-316"><a href="http://rscardwp.px-lab.com/how-ipsum-project-web-dolor-sit-amet/">Image Post</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-315"><a href="http://rscardwp.px-lab.com/ipsum-project-web/">Slider Post</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-314"><a href="http://rscardwp.px-lab.com/how-ipsum-project-web-dolor-sit-amet-2/">Video Post</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-317"><a href="http://rscardwp.px-lab.com/web-dolor-sit-amet/">Audio Post</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-318"><a href="http://rscardwp.px-lab.com/how-ipsum-project-web/">Vimeo Post</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-319"><a href="http://rscardwp.px-lab.com/how-ipsum-project/">Youtube Post</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-320"><a href="http://rscardwp.px-lab.com/disneys-alice-in-wonderland/">Dailymotion Post</a></li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-311"><a href="http://rscardwp.px-lab.com/the-history-and-future-of-motorcycles/">Without Media Post</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-306"><a href="index.php#calendar">Calendar</a></li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-307"><a href=" ">Contact</a></li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-425"><a href="#">Other</a>
                                            <ul class="sub-menu">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-312"><a href="http://rscardwp.px-lab.com/typography-page/">Typography Page</a></li>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-308"><a href="http://rscardwp.px-lab.com/404">404 Page</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-560"><a href="https://px-lab.com/rscard-previews/">All Demos</a>
                                            <ul class="sub-menu">
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-561"><a href="https://themeforest.net/item/resume-cv-vcard-theme/14502131-lab?ref=PX-lab">Buy WordPress version</a></li>
                                            </ul>
                                        </li>
                                    </ul></nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="content">
        <div class="container">    <section id="about" class="section section-about">
                <div class="animate-up">
                    <div class="section-box">
                        <div class="profile">
                            <div class="row">
                                <div class="col-xs-5">
                                    <div class="profile-photo">
                                        <img src="view/admin/basic_info/uploads/<?php echo $info['img']?>" alt=""/></div>
                                </div>
                                <div class="col-xs-7">
                                    <div class="profile-info">
                                        <div class="profile-items clearfix">
                                            <div class="profile-preword"><span>Hello</span></div>

                                        </div>
                                        <h1 class="profile-title">
                                            <span>I&#039;m</span>
                                            <?php
                                            echo $info['name'];
                                            ?>								</h1>
                                        <h2 class="profile-position"><?php
                                            echo $info['name_slug'];
                                            ?></h2></div>
                                    <ul class="profile-list">
                                        <li class="clearfix">
                                            <strong class="title">Age</strong>
                                            <span class="cont"><?php
                                                echo $info['age'];
                                                ?></span>
                                        </li>
                                        <li class="clearfix">
                                            <strong class="title">Address</strong>
                                            <span class="cont"><?php
                                                echo $info['address'];
                                                ?></span>
                                        </li>
                                        <li class="clearfix">
                                            <strong class="title">E-mail</strong>
                                            <span class="cont"><?php
                                                echo $info['email'];
                                                ?></span>
                                        </li>
                                        <li class="clearfix">
                                            <strong class="title">Phone</strong>
                                            <span class="cont"><?php
                                                echo $info['phone'];
                                                ?></span>
                                        </li>
                                        <li class="clearfix">
                                            <strong class="title">Freelance</strong>
                                            <span class="cont"><?php
                                                echo 'till'.$info['freelance'];
                                                ?></span>
                                        </li>
                                        <li class="clearfix">
                                            <strong class="title"><span class="button">Vacation</span></strong>
                                            <span class="cont">
													<i class="rsicon rsicon-calendar"></i><?php
                                                echo 'till'.$info['vacation'];
                                                ?></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div  class="profile-social">
                            <ul  class="social">
                                <li>

                                    <a href="<?php echo $info['twitter']?> "  target="_blank" > <img src="assets/admin/picture/twitter1.jpg"  alt=""></i></a></li>


                                <li><a href=" <?php echo $info['facebook']?>" target="_blank"><img src="assets/admin/picture/faceboks1.jpg"  alt=""></a></li>
                                <!--<li><a href="https://dribbble.com/" target="_blank"><i class="rsicon rsicon-dribbble"></i></a></li>-->
                                <li><a href="<?php echo $info['linkedin']?>" target="_blank"> <img src="assets/admin/picture/linkedin1.jpg"  alt=""></a></li>
                                <li><a href="<?php echo $info['instragram']?> " target="_blank"> <img src="assets/admin/picture/inst.jpg"  alt=""></a></li>
                                <!-- <li><a href="https://plus.google.com/" target="_blank"><i class="rsicon rsicon-google-plus"></i></a></li>
                                 <li><a href="http://xing.com/" target="_blank"><i class="rsicon rsicon-xing"></i></a></li>-->
                            </ul>					</div>
                    </div>

                    <div class="section-txt-btn">

                        <p><a class="btn btn-lg btn-border ripple" target="_blank" href="http://bit.ly/1P5CMQ3">
                                Download Resume						</a></p>
                        <p><?php echo $info['download']?></p>

                    </div>
                </div>
            </section>

            <section id="skills" class="section section-skills">
                <div class="animate-up">
                    <h2 class="section-title">Professional  Skills</h2>
                    <div class="section-box">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title"><?php echo $skill['skill1']?></span>

                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="<?php echo $skill['percentage1']?>"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title"><?php echo $skill['skill2']?></span>

                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="<?php echo $skill['percentage2']?>"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title"><?php echo $skill['skill3']?></span>

                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="<?php echo $skill['percentage3']?>"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title"><?php echo $skill['skill4']?></span>

                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="<?php echo $skill['percentage4']?>"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title"><?php echo $skill['skill5']?></span>

                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="<?php echo $skill['percentage5']?>"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title"><?php echo $skill['skill6']?></span>

                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="<?php echo $skill['percentage6']?>"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="portfolio" class="section section-portfolio">
                <div class="animate-up">
                    <h2 class="section-title">Portfolio</h2>
                    <div class="section-box-transparent">
                        <div class="filter">
                            <div class="filter-inner">
                                <div class="filter-btn-group">
                                    <button data-filter="*">All</button>
                                    <button data-filter=".photography">Photography</button>
                                    <!--																							<button data-filter=".ux-design">UX design</button>-->
                                </div>
                                <div class="filter-bar">
                                    <span class="filter-bar-line"></span>
                                </div>
                            </div>
                        </div>

                        <div class="pf-grid pf-container" data-popup="type2" data-view="grid">
                            <div class="pf-grid-sizer"></div>															<div class="pf-grid-item size22 photography ">
                                <article class="pf-item"  style="background-image: url('http://rscardwp.px-lab.com/wp-content/uploads/2015/12/photo-1422480041422-130238c67fe2-1-606x597.jpg')">
                                    <div class="pf-item-cont text-left clear-mrg">
                                        <img class="pf-item-logo" src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/logo-jquery-1.png" alt="">

                                        <h2 class="pf-item-title">Dreams visualization</h2>

                                        <div class="pf-item-cat">
                                            <span>Photography </span>											</div>
                                    </div>
                                    <div class="pf-btn-group text-left">
                                        <a class="pf-btn pf-btn-zoom" href="#"><i class="rsicon rsicon-zoom_in"></i></a>
                                        <a class="pf-btn pf-btn-eye" href="http://rscardwp.px-lab.com/portfolio/street-photography/"><i class="rsicon rsicon-eye"></i></a>
                                        <a target="_blank" class="pf-btn pf-btn-link" href="http://photodune.net/?osr=tn&amp;_ga=1.93421085.1097235765.1428662395"><i class="rsicon rsicon-link"></i></a>
                                    </div>

                                    <div class="pf-popup-slide">
                                        <div class="pf-popup-item">

                                            <div class="pf-popup-media">
                                                <div class="pf-embed" data-type="image" data-width="670" data-height="470" data-url="http://rscardwp.px-lab.com/wp-content/uploads/2015/12/portfolio-thumb-09-610x600-1.jpg"></div>
                                            </div>
                                            <div class="pf-embed" data-type="image" data-width="670" data-height="470" data-url="http://rscardwp.px-lab.com/wp-content/uploads/2015/12/portfolio-thumb-08-610x600-1.jpg"></div>


                                            <div class="pf-popup-info">
                                                <header class="pf-popup-head clear-mrg">
                                                    <h2 class="pf-popup-title">Dreams visualization</h2>
                                                    <h3 class="pf-popup-cat"><span>Photography </span></h3>
                                                </header>

                                                <div class="pf-popup-cont">
                                                    <dl class="pf-popup-def-list">
                                                        <dt>Client</dt>
                                                        <dd>Client name</dd>
                                                        <dt>Project</dt>
                                                        <dd>Project name</dd>
                                                        <dt>Other title</dt>
                                                        <dd>Description</dd>
                                                    </dl>

                                                    <div class="pf-popup-text clear-mrg">
                                                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="pf-grid-item size11 photography ux-design ">
                                <article class="pf-item"  style="background-image: url('http://rscardwp.px-lab.com/wp-content/uploads/2015/12/kaboompics-293x285.jpg')">
                                    <div class="pf-item-cont text-left clear-mrg">

                                        <h2 class="pf-item-title">Time is Chronos</h2>

                                        <div class="pf-item-cat">
                                            <span>Photography </span>
                                            <!--                                                <span>UX design </span>	-->
                                        </div>
                                    </div>
                                    <div class="pf-btn-group text-left">
                                        <a class="pf-btn pf-btn-zoom" href="#"><i class="rsicon rsicon-zoom_in"></i></a>
                                        <a target="_blank" class="pf-btn pf-btn-link" href="http://photodune.net/?osr=tn&amp;_ga=1.93421085.1097235765.1428662395"><i class="rsicon rsicon-link"></i></a>
                                    </div>

                                    <div class="pf-popup-slide">
                                        <div class="pf-popup-item">

                                            <div class="pf-popup-media">
                                                <div class="pf-embed" data-type="image" data-width="670" data-height="470" data-url="http://rscardwp.px-lab.com/wp-content/uploads/2015/12/portfolio-thumb-02-610x6001-1.jpg"></div>
                                            </div>


                                            <div class="pf-popup-info">
                                                <header class="pf-popup-head clear-mrg">
                                                    <h2 class="pf-popup-title">Time is Chronos</h2>
                                                    <h3 class="pf-popup-cat"><span>Photography </span>
                                                        <!--                                                                <span>UX design </span>-->
                                                    </h3>
                                                </header>

                                                <div class="pf-popup-cont">
                                                    <dl class="pf-popup-def-list">
                                                        <dt>Client</dt>
                                                        <dd>Client name</dd>
                                                        <dt>Project</dt>
                                                        <dd>Project name</dd>
                                                        <dt>Other title</dt>
                                                        <dd>Description</dd>
                                                    </dl>

                                                    <div class="pf-popup-text clear-mrg">
                                                        <p>Far far away, behind the word mountains, far from </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="pf-grid-item size11 ux-design ">
                                <article class="pf-item"  style="background-image: url('http://rscardwp.px-lab.com/wp-content/uploads/2015/12/photo-1421757295538-9c80958e75b0-293x285.jpg')">
                                    <div class="pf-item-cont text-left clear-mrg">

                                        <h2 class="pf-item-title">Office selection</h2>

                                        <div class="pf-item-cat">
                                            <!--												<span>UX design </span>	-->
                                        </div>
                                    </div>
                                    <div class="pf-btn-group text-left">
                                        <a class="pf-btn pf-btn-zoom" href="#"><i class="rsicon rsicon-zoom_in"></i></a>
                                        <a target="_blank" class="pf-btn pf-btn-link" href="http://photodune.net/?osr=tn&amp;_ga=1.93421085.1097235765.1428662395"><i class="rsicon rsicon-link"></i></a>
                                    </div>

                                    <div class="pf-popup-slide">
                                        <div class="pf-popup-item">

                                            <div class="pf-popup-media">
                                                <div class="pf-embed" data-type="image" data-width="670" data-height="470" data-url="http://rscardwp.px-lab.com/wp-content/uploads/2015/12/portfolio-thumb-03-610x600-1.jpg"></div>
                                            </div>
                                            <div class="pf-embed" data-type="iframe" data-width="670" data-height="470" data-url="https://www.youtube.com/embed/mZb_gat5YCY"></div>


                                            <div class="pf-popup-info">
                                                <header class="pf-popup-head clear-mrg">
                                                    <h2 class="pf-popup-title">Office selection</h2>
                                                    <h3 class="pf-popup-cat">
                                                        <!--                                                                <span>UX design </span>-->
                                                    </h3>
                                                </header>

                                                <div class="pf-popup-cont">
                                                    <dl class="pf-popup-def-list">
                                                        <dt>Client</dt>
                                                        <dd>Client name</dd>
                                                        <dt>Project</dt>
                                                        <dd>Project name</dd>
                                                        <dt>Other title</dt>
                                                        <dd>Description</dd>
                                                    </dl>

                                                    <div class="pf-popup-text clear-mrg">
                                                        <p>Far far away, behind the word mountains, far from the countries</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>

                        </div>
                        <div class="pf-grid-more text-center"
                             data-cat = "[10,11]"
                             data-tax = "1"
                             data-path = "http://rscardwp.px-lab.com/var/www/pxlab-subdomains/rscardwp/wp-content/themes/rs-card/inc/components/portfolio.php"
                             data-count = "2"
                             data-more-count="3"
                             data-offset="3"
                             data-popup_style="cross_popup"
                             data-display_type="grid"
                             data-loaded-count = "0" >

                            <span class="ajax-loader"></span>
                            <button class="btn btn-border ripple"><i class="rsicon rsicon-add"></i></button>
                        </div>
                    </div>
                </div>
            </section>
            <section id="services" class="section section-services text-left">
                <div class="animate-up">
                    <h2 class="section-title">Our Services</h2>
                    <div class="section-box">

                        <div class="row">
                            <div class="col-sm-4">
                                <div class="service">
                                    <div class="service-icon"><i class="rsicon rsicon-paint-brush"></i></div>
                                    <h3 class="service-title"><?php echo $service['title4'] ?></h3>
                                    <h4 class="service-sub-title"><?php echo $service['session4'] ?></h4>
                                    <div class="service-cont">
                                        <hr>
                                        <p><?php echo $service['details4'] ?></p>
                                    </div>
                                </div>
                            </div>
                            <div
                            <div class="col-sm-4">
                                <div class="service">
                                    <div class="service-icon"><i class="rsicon rsicon-send"></i></div>
                                    <h3 class="service-title"><?php echo $service['title1'] ?></h3>
                                    <h4 class="service-sub-title"><?php echo $service['session1'] ?></h4>
                                    <div class="service-cont">
                                        <hr>
                                        <p><?php echo $service['details1'] ?> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="service">
                                    <div class="service-icon"><i class="rsicon rsicon-shopping-bag"></i></div>
                                    <h3 class="service-title"><?php echo $service['title2'] ?></h3>
                                    <h4 class="service-sub-title"><?php echo $service['session2'] ?></h4>
                                    <div class="service-cont">
                                        <hr>
                                        <p><?php echo $service['details2'] ?></p>
                                    </div>
                                </div>
                            </div>
                            <!--																				<div class="col-sm-4">-->
                            <!--												<div class="service">-->
                            <!--																											<div class="service-icon"><i class="rsicon rsicon-wordpress"></i></div>-->
                            <!--																																								<h3 class="service-title">Wordpress</h3>-->
                            <!--																																								<h4 class="service-sub-title">UI and UX design</h4>-->
                            <!--																																								<div class="service-cont">-->
                            <!--															<hr>-->
                            <!--															<p>Eczema can be a burden. It is a skin condition that can range in severity from dry, red patches on your skin to cracked, weeping sores. </p>-->
                            <!--														</div>-->
                            <!--																									</div>-->
                            <!--											</div>-->
                            <div class="col-sm-4">
                                <div class="service">
                                    <div class="service-icon"><i class="rsicon rsicon-life-bouy"></i></div>
                                    <h3 class="service-title"><?php echo $service['title3'] ?></h3>
                                    <h4 class="service-sub-title"><?php echo $service['session3'] ?></h4>
                                    <div class="service-cont">
                                        <hr>
                                        <p><?php echo $service['details3'] ?>.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="experience" class="section section-experience">
                <div class="animate-up">
                    <h2 class="section-title">Work Experience</h2>
                    <div class="timeline">
                        <div class="timeline-bar"></div>
                        <div class="timeline-inner clearfix">
                            <div class="timeline-box timeline-box-right">
                                <span class="dot"></span>
                                <div class="timeline-box-inner animate-left">
                                    <span class="arrow"></span>
                                    <div class="timeline-head">
                                        <div class="date"><?php echo $experience['session1']?></div>
                                        <a class="img" href="#">
                                            <img  src=" " alt="">
                                        </a>
                                        <h4><?php echo $experience['title1']?></h4>
                                    </div>
                                    <p><?php echo $experience['details1']?></p>
                                </div>
                            </div>
                            <div class="timeline-box timeline-box-right">
                                <span class="dot"></span>
                                <div class="timeline-box-inner animate-left">
                                    <span class="arrow"></span>
                                    <div class="timeline-head">
                                        <div class="date"><?php echo $experience['session1']?></div>
                                        <a class="img" href="#">
                                            <img  src=" " alt="">
                                        </a>
                                        <h4><?php echo $experience['title1']?></h4>
                                    </div>
                                    <p><?php echo $experience['details1']?></p>
                                </div>
                            </div>

                            <div class="timeline-box timeline-box-left">
                                <span class="dot"></span>
                                <div class="timeline-box-inner animate-right">
                                    <span class="arrow"></span>
                                    <div class="timeline-head">
                                        <div class="date"><?php echo $experience['session2']?></div>
                                        <a class="img" href="#">
                                            <img  src=" " alt="">
                                        </a>
                                        <h4><?php echo $experience['title2']?></h4>
                                    </div>
                                    <p><?php echo $experience['details2']?></p>
                                </div>
                            </div>

                            <div class="timeline-box timeline-box-left">
                                <span class="dot"></span>
                                <div class="timeline-box-inner animate-right">
                                    <span class="arrow"></span>
                                    <div class="timeline-head">
                                        <div class="date"><?php echo $experience['session3']?></div>
                                        <a class="img" href="#">
                                            <img  src=" " alt="">
                                        </a>
                                        <h4><?php echo $experience['title3']?></h4>
                                    </div>
                                    <p><?php echo $experience['details3']?></p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
        </div>
        </section>



        <section id="text_section" class="section section-text">
            <div class="animate-up">
                <h2 class="section-title">Twitter section</h2>
                <div class="section-box-transparent">
                    <div class="twitter-icon">
                        <i class="rsicon rsicon-twitter"></i>
                    </div><div class="latest-tweets"><div><p class="tweet-text"><a href="http://twitter.com/search?q=%23WP">#WP</a> <a href="http://twitter.com/search?q=%23plugins">#plugins</a> to set the <a href="http://twitter.com/search?q=%23theme">#theme</a> – best <a href="http://twitter.com/search?q=%23time">#time</a>-savers!

                                <a href="https://t.co/vrYRCCFjhR">https://t.co/vrYRCCFjhR</a> <a href="https://t.co/JvS2cbuPR9">https://t.co/JvS2cbuPR9</a></p><p class="tweet-details"><a target="_blank" href="http://twitter.com/_pxlab/status/924952451769389057"><time datetime="2016-07-21 07:43:32+0000">October 30, 2017  2:53 pm</time></a></p></div><div><p class="tweet-text">Have you already checked <a href="http://twitter.com/search?q=%23Boombox">#Boombox</a> new <a href="http://twitter.com/search?q=%23demo">#demo</a>

                                <a href="https://t.co/3JcG9icroa">https://t.co/3JcG9icroa</a>

                                Buy Boombox on <a href="http://twitter.com/search?q=%23themeforest">#themeforest</a> -… <a href="https://t.co/iELMn1Qbhw">https://t.co/iELMn1Qbhw</a></p><p class="tweet-details"><a target="_blank" href="http://twitter.com/_pxlab/status/923819340193058816"><time datetime="2016-07-21 07:43:32+0000">October 27, 2017 11:51 am</time></a></p></div></div>
                </div>
            </div>
        </section>
        <section id="pricing" class="section section-prices">
            <div class="animate-up">
                <h2 class="section-title">Pricing</h2>

                <div class="row price-list">
                    <div class="col-sm-4">
                        <div class="price-box">
                            <div class="price-box-top">
                                <span>$16</span>
                                <small>/mo</small>
                            </div>
                            <div class="price-box-content">
                                <h3>Basic</h3>
                                <ul>
                                    <li>Lorem Ipsum Servise</li>
                                    <li>12 Ipsum Servise</li>
                                    <li><del>Lorem Ipsum Servise 6</del></li>
                                    <li><del>New Servise 2I</del></li>
                                    <li><del>Servise</del></li>
                                    <li><del>Ipsum Servise 2</del></li>
                                </ul>
                                <div class="btn-wrap">
                                    <a class="btn btn-lg btn-border" href="#">
                                        Buy Now																												</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="price-box box-primary">
                            <div class="price-box-top">
                                <span>$50</span>
                                <small>/mo</small>
                            </div>
                            <div class="price-box-content">
                                <h3>Professional</h3>
                                <ul>
                                    <li>Lorem Ipsum Servise</li>
                                    <li>48 Ipsum Servise 2</li>
                                    <li>Lorem Ipsum Servise 6</li>
                                    <li>New Servise 2I</li>
                                    <li><del>Servise</del></li>
                                    <li><del>Ipsum Servise 2</del></li>
                                </ul>
                                <div class="btn-wrap">
                                    <a class="btn btn-lg btn-border" href="#">
                                        Buy Now																												</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="price-box">
                            <div class="price-box-top">
                                <span>$600</span>
                                <small>/mo</small>
                            </div>
                            <div class="price-box-content">
                                <h3>Enterprise</h3>
                                <ul>
                                    <li>Lorem Ipsum Servise</li>
                                    <li>48 Ipsum Servise 2</li>
                                    <li>Lorem Ipsum Servise 6</li>
                                    <li>New Servise 2I</li>
                                    <li>Servise <span class="new">new</span></li>
                                    <li>Ipsum Servise 2 <span class="new">new</span></li>
                                </ul>
                                <div class="btn-wrap">
                                    <a class="btn btn-lg btn-border" href="#">
                                        Buy Now																												</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section-txt-btn"><p>I am pretty flexible and ready to move forward to my clients with additional requirements or with changing existing requirements. For any additional packages please contact me <a href="#">directly</a>.</p>
                </div>
            </div>
        </section>
        <!--				 					<section id="text_section" class="section section-text">-->
        <!--						<div class="animate-up">-->
        <!--															<h2 class="section-title">WooCommerce Products</h2>-->
        <!--																						<div class="section-box-transparent">-->
        <!--									<div class="woocommerce columns-3">-->
        <!---->
        <!--			<ul class="products row">-->
        <!---->
        <!---->
        <!--					<li class="col-md-4 col-sm-6 col-xs-12">-->
        <!--	<div class="product post-box">-->
        <!--		<div class="product-media"><img width="300" height="300" src="http://rscardwp.px-lab.com/wp-content/uploads/2013/06/cd_2_angle-300x300.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="cd_2_angle" title="cd_2_angle" srcset="http://rscardwp.px-lab.com/wp-content/uploads/2013/06/cd_2_angle-300x300.jpg 300w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/cd_2_angle-150x150.jpg 150w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/cd_2_angle-768x768.jpg 768w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/cd_2_angle-180x180.jpg 180w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/cd_2_angle-600x600.jpg 600w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/cd_2_angle-200x200.jpg 200w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/cd_2_angle.jpg 1000w" sizes="(max-width: 300px) 100vw, 300px" /><div class="product-links"></a><a rel="nofollow" href="http://rscardwp.px-lab.com/?add-to-cart=87" data-quantity="1" data-product_id="87" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to cart</a></div></div><div class="product-content">			<h3>-->
        <!--				<a href="http://rscardwp.px-lab.com/product/woo-album-2/">-->
        <!--					Woo Album #2				</a>-->
        <!--			</h3>-->
        <!---->
        <!--			<div class="product-price">-->
        <!--	<div class="star-rating" title="Rated 4 out of 5"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div>-->
        <!--	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>9.00</span></span>-->
        <!--</div></div>	</div>-->
        <!--</li>-->
        <!---->
        <!---->
        <!--					<li class="col-md-4 col-sm-6 col-xs-12">-->
        <!--	<div class="product post-box">-->
        <!--		<div class="product-media"><img width="300" height="300" src="http://rscardwp.px-lab.com/wp-content/uploads/2013/06/hoodie_5_front-300x300.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="hoodie_5_front" title="hoodie_5_front" srcset="http://rscardwp.px-lab.com/wp-content/uploads/2013/06/hoodie_5_front-300x300.jpg 300w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/hoodie_5_front-150x150.jpg 150w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/hoodie_5_front-768x768.jpg 768w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/hoodie_5_front-180x180.jpg 180w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/hoodie_5_front-600x600.jpg 600w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/hoodie_5_front-200x200.jpg 200w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/hoodie_5_front.jpg 1000w" sizes="(max-width: 300px) 100vw, 300px" /><div class="product-links"></a><a rel="nofollow" href="http://rscardwp.px-lab.com/?add-to-cart=56" data-quantity="1" data-product_id="56" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to cart</a></div></div><div class="product-content">			<h3>-->
        <!--				<a href="http://rscardwp.px-lab.com/product/ninja-silhouette-2/">-->
        <!--					Ninja Silhouette				</a>-->
        <!--			</h3>-->
        <!---->
        <!--			<div class="product-price">-->
        <!--	<div class="star-rating" title="Rated 4 out of 5"><span style="width:80%"><strong class="rating">4</strong> out of 5</span></div>-->
        <!--	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>35.00</span></span>-->
        <!--</div></div>	</div>-->
        <!--</li>-->
        <!---->
        <!---->
        <!--					<li class="col-md-4 col-sm-6 col-xs-12">-->
        <!--	<div class="product post-box">-->
        <!--		<div class="product-media"><img width="300" height="300" src="http://rscardwp.px-lab.com/wp-content/uploads/2013/06/hoodie_3_front-300x300.jpg" class="attachment-shop_catalog size-shop_catalog wp-post-image" alt="hoodie_3_front" title="hoodie_3_front" srcset="http://rscardwp.px-lab.com/wp-content/uploads/2013/06/hoodie_3_front-300x300.jpg 300w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/hoodie_3_front-150x150.jpg 150w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/hoodie_3_front-768x768.jpg 768w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/hoodie_3_front-180x180.jpg 180w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/hoodie_3_front-600x600.jpg 600w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/hoodie_3_front-200x200.jpg 200w, http://rscardwp.px-lab.com/wp-content/uploads/2013/06/hoodie_3_front.jpg 1000w" sizes="(max-width: 300px) 100vw, 300px" /><div class="product-links"></a><a rel="nofollow" href="http://rscardwp.px-lab.com/?add-to-cart=50" data-quantity="1" data-product_id="50" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to cart</a></div></div><div class="product-content">			<h3>-->
        <!--				<a href="http://rscardwp.px-lab.com/product/patient-ninja/">-->
        <!--					Patient Ninja				</a>-->
        <!--			</h3>-->
        <!---->
        <!--			<div class="product-price">-->
        <!--	<div class="star-rating" title="Rated 4.67 out of 5"><span style="width:93.4%"><strong class="rating">4.67</strong> out of 5</span></div>-->
        <!--	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>35.00</span></span>-->
        <!--</div></div>	</div>-->
        <!--</li>-->
        <!---->
        <!---->
        <!--			</ul>-->
        <!---->
        <!---->
        <!--			</div>-->
        <!--								</div>-->
        <!--																						<div class="section-txt-btn"><a class="btn btn-lg btn-primary ripple" target="_blank" href="http://rscardwp.px-lab.com/shop">Go To Shop</a>-->
        <!--</div>-->
        <!--													</div>-->
        <!--					</section>-->
        <!--<section id="clients" class="section section-clients">
<div class="animate-up">
                                <h2 class="section-title">My Clients</h2>
                            <div class="section-box-transparent">
                                        <div class="clients-carousel">
                                                        <div class="client-logo">
                                                                        <a href="http://market.envato.com/" target="_blank">
                                                                        <img src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/logo-envato-1.png" alt=""/>
                                                                        </a>

                </div>
                                                        <div class="client-logo">
                                                                        <a href="https://angularjs.org/" target="_blank">
                                                                        <img src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/logo-angularjs-1.png" alt=""/>
                                                                        </a>

                </div>
                                                        <div class="client-logo">
                                                                        <a href="https://www.omniref.com/ruby/gems/teaspoon/0.7.9" target="_blank">
                                                                        <img src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/logo-teaspoon-1.png" alt=""/>
                                                                        </a>

                </div>
                                                        <div class="client-logo">
                                                                        <a href="https://wordpress.com/" target="_blank">
                                                                        <img src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/logo-wordpress-1.png" alt=""/>
                                                                        </a>

                </div>
                                                        <div class="client-logo">
                                                                        <a href="https://evernote.com/?var=1" target="_blank">
                                                                        <img src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/logo-evernote-1.png" alt=""/>
                                                                        </a>

                </div>
                                                        <div class="client-logo">
                                                                        <a href="http://compass-style.org/" target="_blank">
                                                                        <img src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/logo-compass-1.png" alt=""/>
                                                                        </a>

                </div>
                                                        <div class="client-logo">
                                                                        <a href="http://getbootstrap.com/" target="_blank">
                                                                        <img src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/logo-bootstrap-1.png" alt=""/>
                                                                        </a>

                </div>
                                                        <div class="client-logo">
                                                                        <a href="http://jasmine.github.io/" target="_blank">
                                                                        <img src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/logo-jasmine-1.png" alt=""/>
                                                                        </a>

                </div>
                                                        <div class="client-logo">
                                                                        <a href="https://jquery.com/" target="_blank">
                                                                        <img src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/logo-jquery-1.png" alt=""/>
                                                                        </a>

                </div>
                                                </div>
                                </div>
                    </div>
</section>-->
        <!--<section id="references" class="section section-references">
<div class="animate-up">
                            <h2 class="section-title">References</h2>

                                    <div class="section-box">
            <ul class="ref-slider" data-speed="5000">
                                                            <li>
                        <div class="ref-box">
                                                                                    <div class="person-speech"><p>Will send them a message. You ride out as fast as the wind can carry you.You tell the other clans to come. ell them Toruk Macto calls to them! You fly now, with me! My brothers! Sisters!</p>
</div>
                                                                                <div class="person-info clearfix">
                                                                                                                                                            <a href="http://themeforest.net/item/resume-cv-portfolio-wordpress/14502131-lab?ref=PX-lab">
                                                                                                    <img class="person-img" src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/rs-avatar-60x60-1.jpg" alt="Author">
                                                                                                    </a>
                                                                                                                                                        <div class="person-name-title">
                                                                                                            <span class="person-name">
                                                                                                                            <a href="http://themeforest.net/item/resume-cv-portfolio-wordpress/14502131-lab?ref=PX-lab">
                                                                                                                            Tim Jobs																																					</a>
                                                                                                                    </span>
                                                                                                            <span class="person-title">MODERN LLC , HR</span>
                                                                                                    </div>
                                                                                    </div>
                        </div>
                    </li>
                                                            <li>
                        <div class="ref-box">
                                                                                    <div class="person-speech"><p>Will send them a message. You ride out as fast as the wind can carry you.You tell the other clans to come. ell them Toruk Macto calls to them! You fly now, with me! My brothers! Sisters!</p>
</div>
                                                                                <div class="person-info clearfix">
                                                                                                                                                            <a href="http://themeforest.net/item/resume-cv-portfolio-wordpress/14502131-lab?ref=PX-lab">
                                                                                                    <img class="person-img" src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/rs-avatar-60x60-1.jpg" alt="Author">
                                                                                                    </a>
                                                                                                                                                        <div class="person-name-title">
                                                                                                            <span class="person-name">
                                                                                                                            <a href="http://themeforest.net/item/resume-cv-portfolio-wordpress/14502131-lab?ref=PX-lab">
                                                                                                                            Bill Nadella																																					</a>
                                                                                                                    </span>
                                                                                                            <span class="person-title">MODERN LLC , HR</span>
                                                                                                    </div>
                                                                                    </div>
                        </div>
                    </li>
                                                            <li>
                        <div class="ref-box">
                                                                                    <div class="person-speech"><p>Will send them a message. You ride out as fast as the wind can carry you.You tell the other clans to come. ell them Toruk Macto calls to them! You fly now, with me! My brothers! Sisters!</p>
</div>
                                                                                <div class="person-info clearfix">
                                                                                                                                                            <a href="http://themeforest.net/item/resume-cv-portfolio-wordpress/14502131-lab?ref=PX-lab">
                                                                                                    <img class="person-img" src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/rs-avatar-60x60-1.jpg" alt="Author">
                                                                                                    </a>
                                                                                                                                                        <div class="person-name-title">
                                                                                                            <span class="person-name">
                                                                                                                            <a href="http://themeforest.net/item/resume-cv-portfolio-wordpress/14502131-lab?ref=PX-lab">
                                                                                                                            Sergey Page																																					</a>
                                                                                                                    </span>
                                                                                                            <span class="person-title">MODERN LLC , HR</span>
                                                                                                    </div>
                                                                                    </div>
                        </div>
                    </li>
                                                    </ul>
            <div class="ref-slider-nav">
                <span class="slider-prev"></span>
                <span class="slider-next"></span>
            </div>
        </div>
                                                </div>
</section>-->
        <!--<section id="statistics" class="section section-statistics text-left">
<div class="animate-up">
                                    <h2 class="section-title">Any Statistics</h2>
                                <div class="section-box-transparent">
                                                                                    <div class="row">
                                                        <div class="col-sm-3">
                        <div class="statistic">
                                                                                    <div class="statistic-value">1300</div>
                                                                                                                                        <h3 class="statistic-title">
                                                                                                    <i class="rsicon rsicon-smile-o"></i>																														Happy Clients														</h3>
                                                                                                                                        <div class="statistic-cont">
                                    <p>keep clients happy</p>
                                </div>
                                                                            </div>
                    </div>
                                                                                                                            <div class="col-sm-3">
                        <div class="statistic">
                                                                                    <div class="statistic-value">203</div>
                                                                                                                                        <h3 class="statistic-title">
                                                                                                    <i class="rsicon rsicon-folder-open"></i>																														Projects														</h3>
                                                                                                                                        <div class="statistic-cont">
                                    <p>for 4 years</p>
                                </div>
                                                                            </div>
                    </div>
                                                                                                                            <div class="col-sm-3">
                        <div class="statistic">
                                                                                    <div class="statistic-value">145</div>
                                                                                                                                        <h3 class="statistic-title">
                                                                                                    <i class="rsicon rsicon-code"></i>																														Code Rows														</h3>
                                                                                                                                        <div class="statistic-cont">
                                    <p>per hour</p>
                                </div>
                                                                            </div>
                    </div>
                                                                                                                            <div class="col-sm-3">
                        <div class="statistic">
                                                                                    <div class="statistic-value">15000</div>
                                                                                                                                        <h3 class="statistic-title">
                                                                                                    <i class="rsicon rsicon-share"></i>																														Followers														</h3>
                                                                                                                                        <div class="statistic-cont">
                                    <p>per month</p>
                                </div>
                                                                            </div>
                    </div>
                                                    </div>
                                                                        </div>
                            </div>
</section>-->
        <section id="text_section" class="section section-text">
            <div class="animate-up">
                <h2 class="section-title">Awards and Achievements</h2>
                <div class="section-box">
                    <div class='row'><div class='col-sm-6'></p>
                            <h3>ONE HALF</h3>
                            <p>One of the most interesting options is to divide this section to &#8220;One half&#8221; &#8220;One Third&#8221; and &#8220;One Fourth&#8221;. You can use this for Honors or Achievments or Awards sections. You can insert images and photos right in this editor and even add call to action buttons with custom text and links. This is the example of ONE HALF option.</p>
                            <p><a class="btn btn-lg btn-default ripple" target="_blank" href="#">Custom text button</a></p>
                            <p></div><div class='col-sm-6'></p>
                            <h3>ONE HALF</h3>
                            <p>Same options for editing works in here as well. You can <del>change</del> edit and design as you wish. This can be useful in case if you have a lot of sections in resume. You can change theme as you wish with this <strong>powerful and flexible</strong> options.</p>
                            <p>&nbsp;</p>
                            <p><a class="btn btn-lg btn-default ripple" target="_blank" href="#">Custom text button</a></p>
                            <p></div></div>
                </div>
            </div>
        </section>
        <!--<section id="blog" class="section section-blog">
<div class="animate-up">
                            <h2 class="section-title">From The Blog</h2>
                                                        <div class="blog-grid">
        <div class="grid-sizer"></div>

            <div class="grid-item">


<article class="post-174 post type-post status-publish format-video has-post-thumbnail hentry category-uncategorized tag-business tag-city tag-creative post_format-post-format-video post-box">
<div class="post-media ">
            <div class="post-image">
<a href="http://rscardwp.px-lab.com/disneys-alice-in-wonderland/">
<img src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/thumb-449x286-3-1.jpg" alt=""/>

                            <span class="post-type-icon"><i class="rsicon rsicon-play"></i></span>

                    </a>
</div>
</div>

<div class="post-data">
<time class="post-datetime" datetime="2015-12-01T10:22:34+00:00">
<span class="day">01</span>
<span class="month">Dec</span>
</time>

<div class="post-tag">
    <a href="http://rscardwp.px-lab.com/category/uncategorized/">#Uncategorized</a>
</div>

<h3 class="post-title">
<a href="http://rscardwp.px-lab.com/disneys-alice-in-wonderland/">DISNEY&#8217;S ALICE IN WONDERLAND</a>
</h3>

<div class="post-info">
<a href="http://rscardwp.px-lab.com/author/rscard_user/"><i class="rsicon rsicon-user"></i>by rscard_user</a>
    <a href="http://rscardwp.px-lab.com/disneys-alice-in-wonderland/#respond"><i class="rsicon rsicon-comments"></i>0</a>
</div>
</div>
</article>									</div>

            <div class="grid-item">


<article class="post-134 post type-post status-publish format-gallery has-post-thumbnail hentry category-news tag-creative tag-fashion tag-music tag-news post_format-post-format-gallery post-box">
<div class="post-media ">
            <ul class="post-slider">
                    <li><img src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/thumb-449x286-1-1.jpg" alt=""/></li>
            </ul>
<div class="post-slider-arrows">
<div class="slider-prev"></div>
<div class="slider-next"></div>
</div>
</div>

<div class="post-data">
<time class="post-datetime" datetime="2015-11-23T11:45:43+00:00">
<span class="day">23</span>
<span class="month">Nov</span>
</time>

<div class="post-tag">
    <a href="http://rscardwp.px-lab.com/category/news/">#News</a>
</div>

<h3 class="post-title">
<a href="http://rscardwp.px-lab.com/ipsum-project-web/">IPSUM PROJECT WEB</a>
</h3>

<div class="post-info">
<a href="http://rscardwp.px-lab.com/author/rscard_user/"><i class="rsicon rsicon-user"></i>by rscard_user</a>
    <a href="http://rscardwp.px-lab.com/ipsum-project-web/#respond"><i class="rsicon rsicon-comments"></i>0</a>
</div>
</div>
</article>									</div>
                                    </div>
                                                        <div class="section-txt-btn"><a class="btn btn-lg btn-default ripple" target="_blank" href="http://rscardwp.px-lab.com/blog/">Visit My Blog</a>
</div>
                    </div>
</section>-->
        <!--<section id="text_section" class="section section-text">
<div class="animate-up">
                                    <h2 class="section-title">Transparent Text Section</h2>
                                                                <div class="section-box-transparent">
            <p>Hello! I’m Robert Smith and this is custom editor section. You can add here any text or <del>&#8220;Strikethrough&#8221; text </del> and even you can add bulleted or numbered text and even you will be able to add blockquot text. You can align this text to left/right/center.</p>
        </div>
                                                        </div>
</section>-->
        <section id="text_section" class="section section-text">
            <div class="animate-up">
                <h2 class="section-title">Instagram Section</h2>
                <div class="section-box-transparent">
                    <ul class="instagram-feed clearfix"><li>
                            <a target="blank" href="https://www.instagram.com/p/BHwm8xTgpi3/"><img src="assets/front/material-11/scontent.cdninstagram.com/t51.2885-15/e35/13658644_636961293125525_19678889_n.jpg" alt=""></a>
                        </li><li>
                            <a target="blank" href="https://www.instagram.com/p/BHwm7tNAD3x/"><img src="assets/front/material-11/scontent.cdninstagram.com/t51.2885-15/e35/13649117_1768269680058666_216179586_n.jpg" alt=""></a>
                        </li><li>
                            <a target="blank" href="https://www.instagram.com/p/BHwm696Aj-z/"><img src="assets/front/material-11/scontent.cdninstagram.com/t51.2885-15/e35/13597541_1737276826561456_554121326_n.jpg" alt=""></a>
                        </li><li>
                            <a target="blank" href="https://www.instagram.com/p/BHwm6UNgT3z/"><img src="assets/front/material-11/scontent.cdninstagram.com/t51.2885-15/e35/13671789_1734012260143786_1706733133_n.jpg" alt=""></a>
                        </li></ul>
                </div>
                <div class="section-txt-btn"><h4>SLIDER SECTION CREATED WITH SHORTCODE</h4>
                    <div class="slider"><div class='rsslide'><img src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/download.jpg" alt="slide" /></div><div class='rsslide'><img src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/waterf.jpg" alt="slide" /></div><div class='rsslide'><img src="http://rscardwp.px-lab.com/wp-content/uploads/2015/11/photo-1433769778268-24b797c4fc9a.jpg" alt="slide" /></div></div>
                </div>
            </div>
        </section>
        <section id="interests" class="section section-interests">
            <div class="animate-up">
                <h2 class="section-title">My Interests</h2>
                <div class="section-box">
                    <p> <br />
                    </p>
                    <ul class="interests-list">
                        <li>
                            <i class="map-icon map-icon-bicycling"></i>
                            <span><?php echo $interests['interest1']?></span>
                        </li>
                        <li>
                            <i class="map-icon map-icon-movie-theater"></i>
                            <span><?php echo $interests['interest2']?></span>
                        </li>
                        <li>
                            <i class="map-icon map-icon-ice-skating"></i>
                            <span><?php echo $interests['interest3']?></span>
                        </li>
                        <li>
                            <i class="map-icon map-icon-shopping-mall"></i>
                            <span><?php echo $interests['interest4']?></span>
                        </li>
                        <li>
                            <i class="map-icon map-icon-tennis"></i>
                            <span><?php echo $interests['interest5']?></span>
                        </li>


                    </ul>
                </div>
            </div>
        </section><!--
								<section id="text_section" class="section section-text">
						<div class="animate-up">
															<h2 class="section-title">Tabs sections</h2>
																						<div class="section-box">
									<div class="tabs tabs-horizontal"> <ul class="tabs-menu"> <li><a href="#11">Tab 1</a></li> <li><a href="#22">Tab 2</a></li> <li><a href="#33">Tab 3</a></li> <li><a href="#44">Tab 4</a></li> </ul> <div class="tabs-content"> <div id="11" class="tab-content"> Tab 1 content<br />
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sit amet purus urna. Proin dictum fringilla enim, sit amet suscipit dolor dictum in. Maecenas porttitor, est et malesuada congue, ligula elit fermentum massa, sit amet porta odio est at velit. Sed nec turpis neque. Fusce at mi felis, sed interdum tortor. </div> <div id="22" class="tab-content"> Tab 2 content<br />
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sit amet purus urna. Proin dictum fringilla enim, sit amet suscipit dolor dictum in. Maecenas porttitor, est et malesuada congue, ligula elit fermentum massa, sit amet porta odio est at velit. Sed nec turpis neque. Fusce at mi felis, sed interdum tortor. </div> <div id="33" class="tab-content"> Tab 3 content<br />
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sit amet purus urna. Proin dictum fringilla enim, sit amet suscipit dolor dictum in. Maecenas porttitor, est et malesuada congue, ligula elit fermentum massa, sit amet porta odio est at velit. Sed nec turpis neque. Fusce at mi felis, sed interdum tortor. </div> <div id="44" class="tab-content"> Tab 4 content<br />
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sit amet purus urna. Proin dictum fringilla enim, sit amet suscipit dolor dictum in. Maecenas porttitor, est et malesuada congue, ligula elit fermentum massa, sit amet porta odio est at velit. Sed nec turpis neque. Fusce at mi felis, sed interdum tortor. </div> </div> </div>
								</div>
																				</div>
					</section>-->
        <!--<section id="text_section" class="section section-text">
<div class="animate-up">
                                <h2 class="section-title">FAQ with toggle section</h2>
                                                            <div class="section-box-transparent">
        <ul class="togglebox"> <li><h3 class="togglebox-header">Question 1</h3><div class="togglebox-content"> </div></li> <li><h3 class="togglebox-header">Question 2</h3><div class="togglebox-content"> But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their projects again and again. And if she hasn’t been rewritten, then they are still using her.Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. </div></li> <li><h3 class="togglebox-header">Question 3</h3><div class="togglebox-content"> But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure? </div></li><li><h3 class="togglebox-header">Question 4</h3><div class="togglebox-content"> Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way. </div></li> </ul>
    </div>
                                                    </div>
</section>-->
        <section id="calendar" class="section section-calendar">
            <div class="animate-up">
                <h2 class="section-title">Availability Calendar</h2>
                <div class="section-box">
                    <div class="calendar-busy">
                        <div class="calendar-today" style="background: url('http://rscardwp.px-lab.com/wp-content/themes/rs-card/img/rs-calendar-cover.jpg')">
                            <div class="valign-outer">
                                <div class="valign-middle">
                                    <div class="valign-inner">
                                        <div class="date">
                                            <span class="day"></span>
                                            <span class="month"></span>
                                        </div>
                                        <div class="week-day"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="calendar-cont">
                            <div class="calendar-header">
                                <div class="calendar-nav">
                                    <span class="active-date"><span class="active-month"></span><span class="active-year"></span></span>
                                    <a class="calendar-prev ripple-centered" title="Prev"><i class="rsicon rsicon-chevron_left"></i></a>
                                    <a class="calendar-next ripple-centered" title="Next"><i class="rsicon rsicon-chevron_right"></i></a>
                                </div>
                            </div>
                            <input type="hidden" value="2016, 01, 21&amp;2016, 01, 22&amp;2016, 02, 05&amp;2016, 03, 01&amp;2016, 04, 13&amp;2016, 04, 19&amp;2016, 05, 02&amp;2016, 05, 18" class="busy_days_to_js">
                            <table class="calendar-body">
                                <thead class="calendar-thead"></thead>
                                <tbody class="calendar-tbody"></tbody>
                            </table>
                            <div class="calendar-busy-note">Sorry. I&#039;m not available on those days</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="contact" class="section section-contact">
            <div class="animate-up">
                <h2 class="section-title">Contact Us</h2>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="section-box">
                            <h3>Feel free to contact me</h3>
                            <form class='contactForm' action='#' method='post'>
                                <div class='input-field'>
                                    <input class='contact-name' type='text' name='name'/>
                                    <span class='line'></span>
                                    <label>Name</label>
                                </div>

                                <div class='input-field'>
                                    <input class='contact-email' type='email' name='email'/>
                                    <span class='line'></span>
                                    <label>Email</label>
                                </div>

                                <div class='input-field'>
                                    <input class='contact-subject' type='text' name='subject'/>
                                    <span class='line'></span>
                                    <label>Subject</label>
                                </div>

                                <div class='input-field'>
                                    <textarea class='contact-message' rows='4' name='message'></textarea>
                                    <span class='line'></span>
                                    <label>Message</label>
                                </div><div class='g-recaptcha' data-theme='light' data-sitekey='6LcQihQUAAAAAMsDTvWEE9-d79x3A_TOpu0kWUAt' id='recaptcha1'></div><input type='hidden' class='email_to' name='email_to' value='support@px-lab.com'/><span class='btn-outer btn-primary-outer ripple'>
					<input class='contact-submit btn btn-lg btn-primary' type='submit' value='Send'/>
				</span>

                                <div class='contact-response'></div>
                            </form>									</div>
                    </div>
                    <div class="col-sm-6">
                        <div class="section-box contact-info has-map">
                            <ul class="contact-list">
                                <li class="clearfix">
                                    <strong>E-mail</strong>
                                    <span><a href="nbiplob15@gmail.com"><?php echo $contacts['email']?></a></span>
                                </li>
                                <li class="clearfix">
                                    <strong>Phone</strong>
                                    <span><?php echo $contacts['phone']?></span>
                                </li>
                                <li class="clearfix">
                                    <strong>Viber</strong>
                                    <span><?php echo $contacts['viber']?></span>
                                </li>
                                <li class="clearfix">
                                    <strong>FACEBOOK</strong>
                                    <span><?php echo $contacts['facebook']?></span>
                                </li>
                                <li class="clearfix">
                                    <strong>Address</strong>
                                    <span><?php echo $contacts['address']?></span>
                                </li>
                            </ul>
                            <div id="map"

                                 href=" "></div>
                        </div>
                    </div>
                </div>
                <div class="section-txt-btn"><a class="btn btn-lg btn-default ripple" target="_blank" href="http://themeforest.net/item/multiprofile-resume-cv-portfolio-wordpress/14502131?ref=PX-lab">Buy WordPress Version</a>
                </div>
            </div>
        </section>




    </div>
</div>
<?php
include_once 'view/front/includes/footer.php';
?>
