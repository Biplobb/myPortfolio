<?php


namespace App\admin;


use App\connection;
use PDO;
use PDOException;

class basicInfo extends connection
{
    private $id;
    private $name;
    private $name_slug;
    private $age;
    private $address;
    private $email;
    private $phone;
    private $img;
    private $freelance;
    private $vacation;
    private $facebook;
    private $twitter;
    private $linkedin;
    private $instragram;
    private $cart;
    private $download;




    public function set($data = array()){
        if(array_key_exists('id',$data)){
            $this->id= $data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('name_slug',$data)){
            $this->name_slug = $data['name_slug'];
        }
        if(array_key_exists('age',$data)){
            $this->age = $data['age'];
        }
        if(array_key_exists('address',$data)){
            $this->address = $data['address'];
        }
        if(array_key_exists('email',$data)){
            $this->email = $data['email'];
        }
        if(array_key_exists('phone',$data)){
            $this->phone = $data['phone'];
        }
        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('img',$data)){
            $this->img = $data['img'];
        }
        if(array_key_exists('freelance',$data)){
            $this->freelance= $data['freelance'];
        }
        if(array_key_exists('vacation',$data)){
            $this->vacation= $data['vacation'];
        }
        if(array_key_exists('facebook',$data)){
            $this->facebook= $data['facebook'];
        }
        if(array_key_exists('twitter',$data)){
            $this->twitter= $data['twitter'];
        }
        if(array_key_exists('linkedin',$data)){
            $this->linkedin= $data['linkedin'];
        }
        if(array_key_exists('instragram',$data)){
            $this->instragram= $data['instragram'];
        }if(array_key_exists('cart',$data)){
            $this->cart= $data['cart'];
        }if(array_key_exists('download',$data)){
            $this->download= $data['download'];
        }

        return $this;
    }
    public function upload_img(){
        $img_name=$_FILES['img']['name'];
        $tmp_name=$_FILES['img']['tmp_name'];
        $new_name=substr(md5(time()),'0','8');
        $ex_name=explode('.',$img_name);
        $name=end($ex_name);
        $_POST['img']=$new_name.'.'.$name;
        move_uploaded_file($tmp_name, '../../../view/admin/basic_info/uploads/'.$_POST['img']);
        return $_POST['img'];
    }
    public function store(){
        try {

            $stmt =  $this->conn->prepare("INSERT INTO `basic_info` (`name`, `name_slug`, `age`, `address`, `email`, `phone`,`img`, `freelance`, `vacation`,`facebook`,`twitter`,`linkedin`,`instragram`,`cart`,`download`,`unique_id`) VALUES
 
 
 (:name,:name_slug,:age,:address,:email,:phone,:img,:freelance,:vacation,:facebook,:twitter,:linkedin,:instragram,:cart,:download,:unique_id)");

            $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);
            $stmt->bindValue(':name_slug', $this->name_slug, PDO::PARAM_STR);
            $stmt->bindValue(':age', $this->age, PDO::PARAM_STR);
            $stmt->bindValue(':address', $this->address, PDO::PARAM_STR);
            $stmt->bindValue(':email', $this->email, PDO::PARAM_STR);
            $stmt->bindValue(':phone', $this->phone, PDO::PARAM_STR);
            $stmt->bindValue(':img', $this->img, PDO::PARAM_STR);
            $stmt->bindValue(':freelance', $this->freelance, PDO::PARAM_STR);
            $stmt->bindValue(':vacation', $this->vacation, PDO::PARAM_STR);
            $stmt->bindValue(':facebook', $this->facebook, PDO::PARAM_STR);
            $stmt->bindValue(':twitter', $this->twitter, PDO::PARAM_STR);
            $stmt->bindValue(':linkedin', $this->linkedin, PDO::PARAM_STR);
            $stmt->bindValue(':instragram', $this->instragram, PDO::PARAM_STR);
            $stmt->bindValue(':cart', $this->cart, PDO::PARAM_STR);
            $stmt->bindValue(':download', $this->download, PDO::PARAM_STR);
            $stmt->bindValue(':unique_id',md5(time()), PDO::PARAM_STR);


            $stmt->execute();



        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        header('location:index.php');
    }
    public function view()
    {
        try {

            $stmt = $this->conn->prepare("SELECT * FROM `basic_info`");

            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        header('location:index.php');
    }
    public function index()
    {
        try {

            $stmt = $this->conn->prepare("SELECT * FROM `basic_info` where `deleted_at`='0000-00-00 00:00:00'");

            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        header('location:index.php');
    }

    public function show($id)
    {
        try {

            $stmt = $this->conn->prepare("SELECT * FROM `basic_info` where unique_id=$id");

            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        header('location:index.php');
    }
    public function delete($id)
    {
        try {

            $stmt = $this->conn->prepare("Delete FROM `basic_info` where unique_id=$id");

            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        header('location:index.php');
    }public function update()
    {
        try {

            $stmt = $this->conn->prepare("UPDATE `basic_info` SET `name` =:name,`name_slug` =:name_slug,`age` =:age,`address` =:address,`email` =:email,`phone` =:phone,`freelance` =:freelance,`vacation` =:vacation,`facebook`=:facebook,`twitter`=:twitter,`linkedin`=:linkedin,`instragram`=:instragram,`cart`=:cart,`download`=:download,`img` =:img WHERE `basic_info`.`id` = :id");
            $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);
            $stmt->bindValue(':name_slug', $this->name_slug, PDO::PARAM_STR);
            $stmt->bindValue(':age', $this->age, PDO::PARAM_STR);
            $stmt->bindValue(':address', $this->address, PDO::PARAM_STR);
            $stmt->bindValue(':email', $this->email, PDO::PARAM_STR);
            $stmt->bindValue(':phone', $this->phone, PDO::PARAM_STR);
            $stmt->bindValue(':freelance', $this->freelance, PDO::PARAM_STR);
            $stmt->bindValue(':vacation', $this->vacation, PDO::PARAM_STR);
            $stmt->bindValue(':facebook', $this->facebook, PDO::PARAM_STR);
            $stmt->bindValue(':twitter', $this->twitter, PDO::PARAM_STR);
            $stmt->bindValue(':linkedin', $this->linkedin, PDO::PARAM_STR);
            $stmt->bindValue(':instragram', $this->instragram, PDO::PARAM_STR);
            $stmt->bindValue(':cart', $this->cart, PDO::PARAM_STR);
            $stmt->bindValue(':download', $this->download, PDO::PARAM_STR);
            $stmt->bindValue(':img', $this->img, PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_STR);
            $stmt->execute();
          header('location:index.php');
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function delete_image($id)
    {
        try {

            $stmt = $this->conn->prepare("SELECT * FROM `basic_info` where unique_id=$id");

            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        header('location:index.php');
    }
    public function tmp_delete($id)
    {
        try {

            $stmt = $this->conn->prepare("UPDATE `basic_info` SET `deleted_at`=Now() WHERE `basic_info`.`unique_id` = :id");
            $stmt->bindValue(':id', $id, PDO::PARAM_STR);

            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        header('location:index.php');
    }
    public function trash()
    {
        try {

            $stmt = $this->conn->prepare("SELECT * FROM `basic_info` where `deleted_at` !='0000-00-00 00:00:00 '");

            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }

    }
    public function restore($id)
    {
        try {

            $stmt = $this->conn->prepare("UPDATE `basic_info` SET `deleted_at`='0000-00-00 00:00:00 ' WHERE `basic_info`.`unique_id` = :id");
            $stmt->bindValue(':id', $id, PDO::PARAM_STR);

            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        header('location:trash.php');
    }
}