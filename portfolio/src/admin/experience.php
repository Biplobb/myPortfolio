<?php

namespace App\admin;


use App\connection;
use PDO;
use PDOException;

class experience extends connection
{
    private $id;
    private $title1;
    private $session1;
    private $details1;
    private $title2;
    private $session2;
    private $details2;
    private $title3;
    private $session3;
    private $details3;
    private $title4;
    private $session4;
    private $details4;

    public function set($data = array())
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('title1', $data)) {
            $this->title1 = $data['title1'];
        }
        if (array_key_exists('session1', $data)) {
            $this->session1 = $data['session1'];
        }
        if (array_key_exists('details1', $data)) {
            $this->details1 = $data['details1'];
        }
        if (array_key_exists('title2', $data)) {
            $this->title2 = $data['title2'];
        }
        if (array_key_exists('session2', $data)) {
            $this->session2 = $data['session2'];
        }
        if (array_key_exists('details2', $data)) {
            $this->details2 = $data['details2'];
        }
        if (array_key_exists('title3', $data)) {
            $this->title3= $data['title3'];
        }
        if (array_key_exists('session3', $data)) {
            $this->session3 = $data['session3'];
        }
        if (array_key_exists('details3', $data)) {
            $this->details3 = $data['details3'];
        }
        if (array_key_exists('title4', $data)) {
            $this->title4= $data['title4'];
        }
        if (array_key_exists('session4', $data)) {
            $this->session4 = $data['session4'];
        }
        if (array_key_exists('details4', $data)) {
            $this->details4 = $data['details4'];
        }

        return $this;
    }
    public function store(){
        try {

            $stmt =  $this->conn->prepare("INSERT INTO `experience` (`title1`,`session1`,`details1`,`title2`,`session2`,`details2`,`title3`,`session3`,`details3`,`title4`,`session4`,`details4`,`unique_id`) VALUES
 
 
 (:title1,:session1,:details1,:title2,:session2,:details2,:title3,:session3,:details3,:title4,:session4,:details4,:unique_id )");

            $stmt->bindValue(':title1', $this->title1 , PDO::PARAM_STR);
            $stmt->bindValue(':session1', $this->session1 , PDO::PARAM_STR);
            $stmt->bindValue(':details1', $this->details1 , PDO::PARAM_STR);                 $stmt->bindValue(':title2', $this->title2 , PDO::PARAM_STR);
            $stmt->bindValue(':session2', $this->session2 , PDO::PARAM_STR);
            $stmt->bindValue(':details2', $this->details2 , PDO::PARAM_STR);$stmt->bindValue(':title3', $this->title3 , PDO::PARAM_STR);
            $stmt->bindValue(':session3', $this->session3 , PDO::PARAM_STR);
            $stmt->bindValue(':details3', $this->details3 , PDO::PARAM_STR);$stmt->bindValue(':title4', $this->title4 , PDO::PARAM_STR);
            $stmt->bindValue(':session4', $this->session4 , PDO::PARAM_STR);
            $stmt->bindValue(':details4', $this->details4 , PDO::PARAM_STR);
            $stmt->bindValue(':unique_id', md5(time()) , PDO::PARAM_STR);




            $stmt->execute();



        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function index()
    {
        try {

            $stmt = $this->conn->prepare("SELECT * FROM `experience`");

            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        header('location:index.php');
    }
}

