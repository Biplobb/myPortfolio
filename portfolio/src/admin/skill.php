<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/3/2017
 * Time: 12:50 AM
 */

namespace App\admin;


use App\connection;
use PDO;
use PDOException;

class skill extends connection
{
    private $id;
    private $skill1;
    private $percentage1;
    private $skill2;
    private $percentage2;
    private $skill3;
    private $percentage3;
    private $skill4;
    private $percentage4;
    private $skill5;
    private $percentage5;
    private $skill6;
    private $percentage6;
    public function set($data = array())
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('skill1', $data)) {
            $this->skill1 = $data['skill1'];
        }
        if (array_key_exists('percentage1', $data)) {
            $this->percentage1 = $data['percentage1'];
        }
        if (array_key_exists('skill2', $data)) {
            $this->skill2 = $data['skill2'];
        }
        if (array_key_exists('percentage2', $data)) {
            $this->percentage2 = $data['percentage2'];
        }
        if (array_key_exists('skill3', $data)) {
            $this->skill3= $data['skill3'];
        }
        if (array_key_exists('percentage3', $data)) {
            $this->percentage3 = $data['percentage3'];
        }
        if (array_key_exists('skill4', $data)) {
            $this->skill4 = $data['skill4'];
        }
        if (array_key_exists('percentage4', $data)) {
            $this->percentage4 = $data['percentage4'];
        }
        if (array_key_exists('skill5', $data)) {
            $this->skill5 = $data['skill5'];
        }
        if (array_key_exists('percentage5', $data)) {
            $this->percentage5= $data['percentage5'];
        }
        if (array_key_exists('skill6', $data)) {
            $this->skill6 = $data['skill6'];
        }
        if (array_key_exists('percentage6', $data)) {
            $this->percentage6 = $data['percentage6'];
        }
        return $this;
    }
    public function store(){
        try {

            $stmt =  $this->conn->prepare("INSERT INTO `professional_skill` (`skill1`,`percentage1`,`skill2`,`percentage2`,`skill3`,`percentage3`,`skill4`,`percentage4`,`skill5`,`percentage5`,`skill6`,`percentage6`,`unique_id`) VALUES
 
 
 (:skill1,:percentage1,:skill2,:percentage2,:skill3,:percentage3,:skill4,:percentage4,:skill5,:percentage5,:skill6,:percentage6,:unique_id)");

            $stmt->bindValue(':skill1', $this->skill1 , PDO::PARAM_STR);
            $stmt->bindValue(':percentage1', $this->percentage1 , PDO::PARAM_STR);
            $stmt->bindValue(':skill2', $this->skill2 , PDO::PARAM_STR);
            $stmt->bindValue(':percentage2', $this->percentage2 , PDO::PARAM_STR);
            $stmt->bindValue(':skill3', $this->skill3 , PDO::PARAM_STR);
            $stmt->bindValue(':percentage3', $this->percentage3 , PDO::PARAM_STR);
            $stmt->bindValue(':skill4', $this->skill4 , PDO::PARAM_STR);
            $stmt->bindValue(':percentage4', $this->percentage4 , PDO::PARAM_STR);
            $stmt->bindValue(':skill5', $this->skill5 , PDO::PARAM_STR);
            $stmt->bindValue(':percentage5', $this->percentage5 , PDO::PARAM_STR);
            $stmt->bindValue(':skill6', $this->skill6 , PDO::PARAM_STR);
            $stmt->bindValue(':percentage6', $this->percentage6 , PDO::PARAM_STR);
            $stmt->bindValue(':unique_id', md5(time()), PDO::PARAM_STR);

            $stmt->execute();



        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function index()
    {
        try {

            $stmt = $this->conn->prepare("SELECT * FROM `professional_skill`");

            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        header('location:index.php');
    }
    public function show($id)
    {
        try {

            $stmt = $this->conn->prepare("SELECT * FROM `professional_skill` where unique_id=$id");

            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        header('location:index.php');
    }
}

