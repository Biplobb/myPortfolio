<?php


namespace App\admin;


use App\connection;
use PDOException;
use PDO;

class contactUs extends connection
{

    private $id;
    private $email;
    private $phone;
    private $viber;
    private $facebook;
    private $address;

    public function set($data = array()){

        if(array_key_exists('email',$data)){
            $this->email = $data['email'];
        }
        if(array_key_exists('phone',$data)){
            $this->phone = $data['phone'];
        }
        if(array_key_exists('viber',$data)){
            $this->viber = $data['viber'];
        }
        if(array_key_exists('facebook',$data)){
            $this->facebook = $data['facebook'];
        }

        if(array_key_exists('address',$data)){
            $this->address = $data['address'];
        }
        if(array_key_exists('id',$data)){
            $this->id= $data['id'];
        }

        return $this;
    }
    public function store(){
        try {

            $stmt =  $this->conn->prepare("INSERT INTO `contact_us` (`email`, `phone`, `viber`, `facebook`,`address`,`unique_id`) VALUES
 
 
 (:email,:phone,:viber,:facebook,:address,:unique_id)");

            $stmt->bindValue(':email', $this->email, PDO::PARAM_STR);
            $stmt->bindValue(':phone', $this->phone, PDO::PARAM_STR);
            $stmt->bindValue(':viber', $this->viber, PDO::PARAM_STR);
            $stmt->bindValue(':facebook', $this->facebook, PDO::PARAM_STR);
            $stmt->bindValue(':address', $this->address, PDO::PARAM_STR);
            $stmt->bindValue(':unique_id', md5(time()), PDO::PARAM_STR);

            $stmt->execute();



        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }header('location:index.php');
    }
    public function view()
    {
        try {

            $stmt = $this->conn->prepare("SELECT * FROM `contact_us`");

            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }header('location:index.php');
    }
    public function show($id)
    {
        try {

            $stmt = $this->conn->prepare("SELECT * FROM `contact_us` where unique_id=$id");

            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function delete($id)
    {
        try {

            $stmt = $this->conn->prepare("DELETE FROM `contact_us` where unique_id=$id");

            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }header('location:index.php');

    }

    public function update(){
        try {
            $stmt = $this->conn->prepare("UPDATE `contact_us` SET `email` =:email,`phone` =:phone,`viber` =:viber,`facebook` =:facebook,`address` =:address WHERE `contact_us`.`id` =:id;");
            $stmt->bindValue(':email', $this->email, PDO::PARAM_STR);
            $stmt->bindValue(':phone', $this->phone, PDO::PARAM_STR);
            $stmt->bindValue(':viber', $this->viber, PDO::PARAM_STR);
            $stmt->bindValue(':facebook', $this->facebook, PDO::PARAM_STR);
            $stmt->bindValue(':address', $this->address, PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_STR);

            $stmt->execute();



        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }header('location:index.php');

    }
    public function index()
    {
        try {

            $stmt = $this->conn->prepare("SELECT * FROM `contact_us`");

            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}